<?php
include ("header.php");
?>

  <title>Conditions générales de vente - PROXIWEB   </title>
    <meta name="description" content="PROXIWEB vous apporte les solutions les plus adaptées à vos besoins et à votre budget pour être visible et lisible sur Internet et mieux développer votre activité. Nous offrons une gamme complète d’expertise en transformation digitale.">
  
          


 

        <section class="section paralbackground page-banner" style="background-image:url('upload/page_banner_01.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title lb">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Conditions générales de vente  <small>Pour plus infos Centre Support </small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Accueil</a></li>
                            <li class="active">Conditions générales de vente </li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

  

        <section class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Email Support</h3>
                                <hr>
                            </div><!-- end big-title -->

                            <div class="email-widget">
                                <ul class="check-list">
                                    <li><a href="#">info@proxiweb.tn</a></li> 
                                </ul><!-- end check -->
                            </div><!-- end email widget -->
                        </div><!-- end wbp -->    
                    </div><!-- end col -->

                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Conditions générales de vente  </h3>
                                <hr>
                            </div><!-- end big-title -->

			
			 <style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	font-size:11.0pt;
	font-family:"Times New Roman",serif;}
@page WordSection1
	{size:595.0pt 842.05pt;
	margin:14.7pt 19.95pt 0cm 19.9pt;}
div.WordSection1
	{page:WordSection1;}
@page WordSection2
	{size:595.0pt 842.05pt;
	margin:14.7pt 19.95pt 0cm 19.9pt;}
div.WordSection2
	{page:WordSection2;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
 
 

<div class=WordSection1>

<p class=MsoNormal style='line-height:10.0pt'><a name=page1></a><span
style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='line-height:18.9pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#919191'>Article 1 - Application des
conditions générales de vente</span></b></p>

<p class=MsoNormal style='line-height:.8pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>Les présentes conditions
générales de vente sont applicables à tous les produits et services vendus par PROXIWEB
ci-après désigné par « l&#700;Agence ». Elles sont applicables dans leur
intégralité pour tout contrat ou commande passés entre l&#700;Agence et ses
clients. Aucune dérogation aux présentes conditions ne pourra être admise sans
accord express et préalable de l&#700;Agence.</span></p>

<p class=MsoNormal style='line-height:6.6pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>En accord avec
l&#700;article L.441-6 du Code du Commerce le présent document présente au
recto le barème des prix unitaires de la commande du client, ainsi que les
réductions de prix si elles ont lieux.</span></p>

<p class=MsoNormal style='line-height:8.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 2 - Commande et délai de
rétractation</span></b></p>

<p class=MsoNormal style='line-height:9.15pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>Toute commande passée auprès
de l&#700;Agence est ferme et définitive pour les clients dès signature de ce
présent contrat. Selon le Code de la Consommation, l&#700;article L-121-20
notamment, précise que tout contrat passé entre professionnels ne donne pas
droit à un délai de rétractation.</span></p>

<p class=MsoNormal style='line-height:9.1pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;'><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#919191'>Article 3 - Propriété
intellectuelle et concession de licence</span></b></p>

<p class=MsoNormal style='line-height:.8pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12pt;font-family:"Arial",sans-serif;color:#929292'>Conformément au Code de la
propriété intellectuelle, l&#700;Agence est seule titulaire des droits
intellectuels du Code Source et de tous les éléments qu'il aura créés pour
réaliser le site internet, à l'exception des données fournies par le client
sous sa seule responsabilité. Pour permettre au client d'exploiter librement le
site dans le cadre de son activité, le prestataire lui concède une licence
d'exploitation sur tous les éléments concernant le site crée : programmes,
études, analyses, documentation, créations graphiques, iconographiques ou
infographiques qui auront été réalisées dans le cadre du présent contrat. Cette
concession de licence n'est consentie que sous condition du paiement intégral
du prix convenu. Cette licence est tacitement reconduite dès lors que le client
s&#700;acquitte du renouvellement annuel des frais liés au Nom de Domaine et à
l&#700;hébergement de sa solution au tarif en vigueur. Les présentes Conditions
Générales ne pourront en aucun cas faire l&#700;objet d&#700;une cession totale
ou partielle, à titre onéreux, gracieux, ou par apport de fonds de commerce, du
fait du Client, sauf accord préalable écrit de l&#700;Agence. A ce titre, les
licences consenties en son application ne pourront être cédées.</span></p>

<p class=MsoNormal style='line-height:16.55pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 4 - Conditions de
règlement</span></b></p>

<p class=MsoNormal style='line-height:6.6pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-.1pt;'><span style='font-size:
12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span
style='font:12.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Mode de paiement: le détail du mode de paiement spécifique à la
commande figure au recto de ce présent document et résulte d&#700;un accord
établi entre le représentant de l&#700;Agence et le client lors de la signature
du présent contrat.</span></p>

<p class=MsoNormal style='line-height:.5pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-.1pt;'><span style='font-size:
12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span
style='font:12.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Retard de paiement: tout retard de paiement pourra donner lieu,
entre autres, à la suspension des commandes en cours et à l&#700;annulation
immédiate des conditions de délai de paiement établies préalablement au retard
de paiement.</span></p>

<p class=MsoNormal style='line-height:.5pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-.1pt;'><span style='font-size:
12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span
style='font:12.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Impayés: en cas d&#700;impayé, le client s&#700;engage à
régulariser sa situation dans un délai maximum d&#700;une semaine. Dans le cas
contraire, le défaut de paiement total ou partiel entrainera de plein droit,
sans mise en demeure préalable, une interruption des services associés à la
commande et le droit de refuser toute nouvelle commande jusqu&#700;à la
régularisation de la situation. Si toutefois aucun accord à l&#700;amiable
n&#700;est conclu entre les parties, une procédure judiciaire pour recouvrement
de créances pourra être engagée par l&#700;Agence. Dans ce cas, les frais de
procédure seront à la charge du client.</span></p>

<p class=MsoNormal style='line-height:3.35pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 5 - Confidentialité</span></b></p>

<p class=MsoNormal style='line-height:3.85pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>l&#700;Agence </span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#919191'>s&#700;engage à respecter une stricte confidentialité</span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'> </span><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>quant à vos données personnelles
et celles de vos clients telles que le Nom, l&#700;adresse et le numéro de
téléphone. Les informations anonymes concernant les produits, dates, villes,
sont utilisées à des fins statistiques pour suivre les évolutions et tendances
du marché ainsi que pour faciliter la gestion de votre point de vente.</span></p>

<p class=MsoNormal style='line-height:9.7pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 6 - Les différentes
formules</span></b></p>

<p class=MsoNormal style='line-height:4.45pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-.1pt;'><span style='font-size:
12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span
style='font:12.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Location: les formules en location comprennent l&#700;abonnement
du Nom de Domaine, du site internet, les mises à jour et le cas échéant le web
mobile.</span></p>

<p class=MsoNormal style='line-height:.25pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.1pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>Toute formule en location suppose
des  frais</span></p>

<span style='font-size:12.0pt;font-family:"Times New Roman",serif'><br
clear=all>
</span>

<p class=MsoNormal style='line-height:1.0pt'></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:8.5pt;font-family:"Arial",sans-serif;color:#929292'>CONDITIONS
GENERALES DE SERVICE</span></b></p>

<p class=MsoNormal align=center style='margin-right:1.0pt;text-align:center;
'><b><span style='font-size:8.5pt;font-family:
"Arial",sans-serif;color:#929292'>SITE WEB ET APPLICATION</span></b></p>

<p class=MsoNormal style='line-height:9.45pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:1.0pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>de mise en place selon le
tarif en vigueur et un abonnement mensuel défini au recto de ce document. En
optant pour la solution en abonnement, le client peut résilier avec un préavis
d'un mois.</span></p>

<p class=MsoNormal style='line-height:.5pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
text-indent:.35pt;'><span style='font-size:12pt;font-family:"Arial",sans-serif;color:#929292'>-<span style='font:12.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:12pt;font-family:"Arial",sans-serif;
color:#929292'>Concession de licence: les formules en concession de licence
comprennent le Nom de Domaine, l&#700;hébergement de la première année(ces deux
éléments doivent être renouvelés chaque année à la date anniversaire du Nom de
Domaine), le site internet et le cas échéant le web mobile et/ou
l&#700;application mobile.</span></p>

<p class=MsoNormal style='line-height:.05pt'><span style='font-size:12pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:1.0pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Les mises à jour et interventions graphiques seront facturées
selon le tarif en vigueur.</span></p>

<p class=MsoNormal style='line-height:8.8pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#919191'>Article 7 - Site internet et application</span></b></p>

<p class=MsoNormal style='line-height:6.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:212.0pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Le site internet inclut les éléments suivants: Conception
graphique et création du site selon un modèle bien choisi</span></p>

<p class=MsoNormal style='line-height:.25pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Intégration du contenu rédactionnel selon  les informations
fournies par le client;</span></p>

<p class=MsoNormal style='line-height:.35pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Intégration des images et du logo;</span></p>

<p class=MsoNormal style='line-height:.25pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Programmation des pages en langage Html et Php;</span></p>

<p class=MsoNormal style='line-height:.35pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Formulaire d&#700;appréciation;</span></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:18.0pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Contact: formulaire de contact vers l&#700;adresse mail fournie
ou créée pour le client;</span></p>

<p class=MsoNormal style='line-height:.25pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Zone d’intervention;</span></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:12.0pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Création d'une base de données pour la gestion des commandes
clients;</span></p>

<p class=MsoNormal style='line-height:.15pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Horaires d&#700;ouverture ;</span></p>

<p class=MsoNormal style='line-height:.35pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Interface administration du site internet;</span></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:15.0pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Une formation au site internet dispensée dans nos locaux ou à
distance.</span></p>

<p class=MsoNormal style='line-height:8.65pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#919191'>Article 8 - Obligations du client</span></b></p>

<p class=MsoNormal style='line-height:6.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-right:8.0pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Pour permettre au prestataire de réaliser sa mission, le client
s'engage à:</span></p>

<p class=MsoNormal style='line-height:.65pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>fournir une documentation commerciale à jour. La documentation
commerciale comprend notamment la liste des produits, des prix et des images
qui figureront sur le site internet et ou l&#700;application, le logo du client
ainsi que les couleurs retenues dans la charte graphique personnalisée. Si le
client ne possède pas de logo, l&#700;Agence se réserve le droit de facturer
des frais de création de Logo en plus de la prestation du site internet et ou
de l&#700;application. Le client est responsable des photos qu&#700;il transmet
à l&#700;Agence en vue de les faire figurer sur le site internet et ou
l&#700;application. Le client doit s&#700;assurer d&#700;avoir les droits
d&#700;images pour exploiter ces photos. Le client est responsable du contenu
du site internet: images, textes, prix et offres commerciales.</span></p>

<p class=MsoNormal style='line-height:.5pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:18.0pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>fournir au prestataire toutes les données sous le format
approprié;</span></p>

<p class=MsoNormal style='line-height:.6pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:4.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>collaborer activement à la réussite du projet en apportant au
prestataire dans les délais utiles toutes les informations et documents
nécessaires à la bonne exécution des prestations;</span></p>

<p class=MsoNormal style='line-height:.6pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:30.0pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12pt;font-family:"Arial",sans-serif;
color:#929292'>se conformer strictement aux préconisations techniques faites
par le prestataire;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:6.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>valider les éléments soumis par le prestataire dans un délai
raisonnable afin de ne pas retarder l&#700;avancement du projet et par voie de
conséquence la date de livraison du projet;</span></p>

<p class=MsoNormal style='line-height:.55pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>valider le lien test envoyé par l&#700;Agence avant sa mise en
ligne;</span></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:8.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>régler dans les délais impartis les sommes dues au prestataire;</span></p>

<p class=MsoNormal style='line-height:.4pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:5.0pt;text-indent:-4.65pt'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span
style='font:12.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>suivre une
formation de prise en main de la solution;</span></p>

<p class=MsoNormal style='line-height:.7pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
text-indent:.35pt;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span style='font:12.0pt "Times New Roman"'>
</span></span><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>s&#700;acquitter de la quittance annuelle de l&#700;hébergement
et du Nom de Domaine à la date anniversaire des Noms de Domaine selon le tarif
en vigueur. Tout manquement à cette obligation entrainera de plein droit une
suspension de la jouissance du site internet et/ou de l&#700;application. Le
cas échéant l&#700;Agence ne pourra être tenue pour responsable en cas de non
disponibilité du ou des Noms de Domaine. De plus des frais s&#700;élevant à 50€
HT (par Nom de Domaine) seront facturés en plus, si le Nom de Domaine est
expiré et doit être renouvelé.</span></p>

<p class=MsoNormal style='line-height:3.8pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><b><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#919191'>Article 9 - Obligations du prestataire</span></b></p>

<p class=MsoNormal style='line-height:3.45pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Le prestataire s'engage à:</span></p>

<p class=MsoNormal style='line-height:.35pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:5.0pt;text-indent:-4.65pt'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span
style='font:12.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>apporter
son savoir-faire dans les règles de l&#700;art ;</span></p>

<p class=MsoNormal style='line-height:.7pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>enregistrer le Nom de Domaine à condition qu&#700;il ne
s&#700;agisse pas d&#700;une marque déposée;</span></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:8.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-indent:.35pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>envoyer un lien test de production pour que le client puisse
suivre l&#700;évolution du projet et y apporter des modifications le cas
échéant;</span></p>

<span style='font-size:12.0pt;font-family:"Times New Roman",serif'><br
clear=all>
</span>

<p class=MsoNormal style='line-height:1.0pt'></p>

<p class=MsoNormal style='line-height:10.0pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='line-height:17.9pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:18.0pt;margin-bottom:
0cm;margin-left:.05pt;margin-bottom:.0001pt;text-indent:-.05pt;line-height:
102%'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>programmer et paramétrer tous les éléments nécessaires à la
bonne consultation du site conformément aux informations transmises par le
client;</span></p>

<p class=MsoNormal style='line-height:.5pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:19.0pt;margin-bottom:
0cm;margin-left:.05pt;margin-bottom:.0001pt;text-indent:-.05pt;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>proposer des images libre de droit acquises par l&#700;Agence;</span></p>

<p class=MsoNormal style='line-height:.6pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:11.0pt;margin-bottom:
0cm;margin-left:.05pt;margin-bottom:.0001pt;text-indent:-.05pt;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>effectuer les tests et vérifier le fonctionnement des éléments
avant le lancement du site ;</span></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:9.0pt;margin-bottom:0cm;
margin-left:.05pt;margin-bottom:.0001pt;text-indent:-.05pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>-<span style='font:12.0pt "Times New Roman"'> </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>assurer la mise en ligne du site internet sur le Nom de Domaine retenu
après validation de tous les éléments par le client.</span></p>

<p class=MsoNormal style='line-height:8.65pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 10 - Paiement en ligne</span></b></p>

<p class=MsoNormal style='line-height:6.35pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Pour le paiement en ligne, le
client a le choix :</span></p>

<p class=MsoNormal style='line-height:.35pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:4.05pt;text-indent:-4.05pt'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#919191'>-<span
style='font:12.0pt "Times New Roman"'> </span></span><span style='font-size:
12.0pt;font-family:"Arial",sans-serif;color:#919191'>d&#700;opter pour une
solution proposée par sa banque ;</span></p>

<p class=MsoNormal style='line-height:.55pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:4.05pt;text-indent:-4.05pt'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#919191'>-<span
style='font:12.0pt "Times New Roman"'> </span></span><span style='font-size:
12.0pt;font-family:"Arial",sans-serif;color:#919191'>d&#700;opter pour la
solution PayPal ;</span></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;line-height:
100%;font-family:"Arial",sans-serif;color:#919191'>Dans tous les cas, les frais
d&#700;installation et de transaction ainsi que l&#700;abonnement à la solution
de paiement seront à la charge du client.</span></p>

<p class=MsoNormal style='line-height:8.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 11 - Livraison</span></b></p>

<p class=MsoNormal style='line-height:6.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:4.0pt;margin-bottom:0cm;
margin-left:.05pt;margin-bottom:.0001pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Toute date de livraison figurant sur le bon de commande n&#700;a
qu&#700;une valeur indicative. l&#700;Agence ne pourra être tenue pour
responsable vis-à-vis du client de toute erreur ou tout retard de livraison
quelle qu&#700;en soit la cause.</span></p>

<p class=MsoNormal style='line-height:.55pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:2.0pt;margin-bottom:0cm;
margin-left:.05pt;margin-bottom:.0001pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Le présent contrat s'appliquera à compter de sa signature par
les parties et restera en vigueur pendant toute la durée de la réalisation de son
objet.</span></p>

<p class=MsoNormal style='line-height:.6pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.0pt;margin-bottom:0cm;
margin-left:.05pt;margin-bottom:.0001pt;'><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Le délai de réalisation est de 5 semaines en moyenne après
validation de la charte graphique et de la maquette de la page d&#700;accueil.
Tout retard imputable au client entraînera une modification corrélative du
délai de livraison. Le délai de livraison dépend de la réactivité du client aux
demandes du prestataire : informations commerciales et validation notamment. La
livraison est effective dès lors que le site internet est mis en ligne sur le
lien final.</span></p>

<p class=MsoNormal style='line-height:8.7pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 12 - Garantie</span></b></p>

<p class=MsoNormal style='line-height:9.15pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;line-height:
100%;font-family:"Arial",sans-serif;color:#929292'>Le client a été informé et
reconnaît qu'en l'état actuel de la technique informatique et de son évolution
très rapide, il est impossible de garantir que le site</span></p>

<p class=MsoNormal style='line-height:.6pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>fonctionnera sans
discontinuité et sans dysfonctionnements; néanmoins le prestataire garantit que
le site sera substantiellement conforme à l&#700;offre commercial du client et
qu'il corrigera toutes les anomalies ou bogues bloquants pendant une durée de
trois mois à compter de la mise en ligne du site internet et ou de
l&#700;application. Cette garantie ne couvrira que les défaillances des
éléments fournis par le prestataire et pour autant que le client ait respecté
les prescriptions d'utilisation du site.</span></p>

<p class=MsoNormal style='line-height:8.45pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt'><b><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#919191'>Article 13 - Responsabilités</span></b></p>

<p class=MsoNormal style='line-height:4.2pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;line-height:
103%;font-family:"Arial",sans-serif;color:#929292'>Le prestataire assurera ses
prestations en respectant les règles de l'art en usage dans la profession; il
est expressément convenu qu'il ne sera tenu qu'à une obligation générale de
moyens. Quels que soient la nature, le fondement et les modalités de
l&#700;action qui pourrait être engagée contre l&#700;Agence, l&#700;indemnité
due au client en réparation de son préjudice éventuel ne pourra pas dépasser la
valeur des services ou de la partie du service en cause. L&#700;Agence ne peut
être tenue pour responsable des manquements ou des retards dans
l&#700;exécution de ses obligations dus à un cas de force majeur, un cas fortuit,
ou tout événement échappant raisonnablement au contrôle de l&#700;Agence.</span></p>

<p class=MsoNormal style='line-height:.4pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-.05pt;'><span style='font-size:
12.0pt;font-family:"Arial",sans-serif;color:#929292'>-<span
style='font:12.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#929292'>Limite de responsabilités : l&#700;Agence ne pourra être tenue
pour responsable concernant des problèmes techniques résultant de
l&#700;activité de ses partenaires et indépendant de sa volonté (hébergement,
prestataire</span></p>

<p class=MsoNormal style='line-height:.6pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt'><span style='font-size:12pt;
font-family:"Arial",sans-serif;color:#929292'>SMS, portail d&#700;achat de Nom
de Domaine notamment).</span></p>

<p class=MsoNormal style='line-height:.8pt'><span style='font-size:12.0pt;
font-family:"Arial",sans-serif;color:#929292'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>Le Client reconnaît par les
présentes conditions que les fluctuations de la bande passante, les aléas du
fournisseur d'accès et la variété des navigateurs et leurs versions sont des
éléments pouvant entraîner un disfonctionnement dans les prestations offertes
par l&#700;Agence qui ne pourra être tenue pour responsable le cas échéant.</span></p>

<p class=MsoNormal style='line-height:4.05pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:25.0pt;margin-bottom:
0cm;margin-left:.05pt;margin-bottom:.0001pt;'><b><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;
color:#919191'>Article 14 - Attribution de juridiction et droit applicable</span></b></p>

<p class=MsoNormal style='line-height:.75pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.05pt;text-align:justify;text-justify:
inter-ideograph;'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#929292'>De façon express, quelque
soit le lieu de commande ou de livraison, il est donné attribution de
juridiction au tribunal de Créteil pour toute contestation pouvant surgir entre
les parties, avec application de la loi française.</span></p>

<p class=MsoNormal style='line-height:14.45pt'><span style='font-size:12.0pt'>&nbsp;</span></p>

</div>

<span style='font-size:11.0pt;font-family:"Times New Roman",serif'><br
clear=all style='page-break-before:auto'>
</span>

<div class=WordSection2>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:6.0pt;font-family:"Arial",sans-serif;color:#B5B5B5'>
Siège social PROXIWEB : Immeuble PROXIWEB<br>41 Avenue Hedi Chaker Lafayette TUNIS</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:6.0pt;font-family:"Arial",sans-serif;color:#B5B5B5'>Téléphone
: 71 891 521 - Email : info@proxiweb.tn - Site Web : www.proxiweb.tn</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:6.0pt;font-family:"Arial",sans-serif;color:#B5B5B5'>MF: 1552671M/A/M/000 - RC : B0129122018</span></b></p>

</div>


                        </div><!-- end wbp -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->




 


<?php 
include ("footer.php");

?>
 