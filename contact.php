<?php
include ("header.php");
?>

  <title>Contactez nous - PROXIWEB - Commandez votre modèle web </title>
    <meta name="description" content="PROXIWEB vous apporte les solutions les plus adaptées à vos besoins et à votre budget pour être visible et lisible sur Internet et mieux développer votre activité. Nous offrons une gamme complète d’expertise en transformation digitale.">
  
          


 

        <section class="section paralbackground page-banner" style="background-image:url('upload/page_banner_01.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title lb">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Contactez nous <small>Pour plus infos Centre Support </small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="#">Accueil</a></li>
                            <li class="active">Contact</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->



        <section class="section lb" style="padding: 0px !important;">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Équipe PROXIWEB en ligne</h3>
                                <hr>
                            </div><!-- end big-title -->

                            <div class="email-widget">
                                <p>Pour vos autres questions, veuillez utiliser les adresses e-mail ci-dessous. Pour les questions de pré-vente, décidons ensemble avec le service approprié pour vous en consultation avec l'équipe de vente.</p>
                               <b> Appelez de 08H / 20H Lun à Sam</b> 
								
									<ul class="check-list">
								<li><a href="tel:71 891 521"><i class="fa fa-phone"></i> 71 891 521 (Service Client)</a></li>
								<li><a href="tel:31 320 300"><i class="fa fa-phone"></i> 31 320 300 (Service Client)</a></li>
								<li><a href="tel:56 074 720"><i class="fa fa-phone"></i> 56 074 720 (Service Client)</a></li>
                       </ul>
						
								<b>Messages par Emails ( Réponse dans 24H max )</b>
							 
								<ul class="check-list">
                                    <li><a href="#"><i class="fa fa-envelope"></i> info@proxiweb.tn</a></li>
                                    
                                </ul><!-- end check -->
                            </div><!-- end email widget -->
                        </div><!-- end wbp -->    
						
						
						
						
						
						
                    </div><!-- end col -->

                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Votre message  </h3>
                                <hr>
                            </div><!-- end big-title -->

			
			 <?php
				 
					
if (isset ($_REQUEST['Envoyer']))
{ 

$type = $_POST['type'];
       $civilite = $_POST['civilite'];
	    $nom= addslashes(htmlentities(($_POST['nom']) ,ENT_QUOTES,'UTF-8' )); 
		 $prenom= addslashes(htmlentities(($_POST['prenom']) ,ENT_QUOTES,'UTF-8' )); 
	   $tel =$_POST['tel'];
	   $mobile= $_POST['mobile'];
	   $email= $_POST['email'];
 
	    $objet= addslashes(htmlentities(($_POST['objet']) ,ENT_QUOTES,'UTF-8' )); 
	   $commentaire= addslashes(htmlentities(($_POST['commentaire']) ,ENT_QUOTES,'UTF-8' )); 
	 
 $response = $_POST["g-recaptcha-response"];
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => '6LfhqfEUAAAAAKY8f1pmrIu1NYDdGZ3BSkvcbwH3',
		'response' => $_POST["g-recaptcha-response"]
	);
	$options = array(
		'http' => array (
			'method' => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context  = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success=json_decode($verify);
 
if ($captcha_success->success==true) {		  
	 
 
?>
 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
	<td>	
		<span class="h5"><b><?php echo _("Nous avons bien re&ccedil;u votre demande , Vous serez contact&eacute; dans les plus brefs d&eacute;lais.
<br>Merci de votre confiance."); ?>
        </b></span><b><br>
        <span class="h5"><br>
        <?php echo _("Cordialement,
<br>L'&eacute;quipe de PROXIWEB."); ?></span></b> 
<br><br>

 </td></tr></table>

<?php
 
     $headers ="From: info@proxiweb.tn"."\n"; 
     $headers .="Content-Type: text/html; charset=\"iso-8859-1\""."\n"; 
     $headers .="Content-Transfer-Encoding: 8bit"; 

$html="<html><head><title>Formulaire de contact PROXIWEB</title></head><body>
 
<font size=3 color='green'><b> Formulaire de contact PROXIWEB <b></font> <br>	<br>  	
Date : <b> ".date('d/m/Y')." &agrave; ".date('H:i')."</b><br>		

Type de contact :  <b>$type</b> <br><br>  
Nom & Pr&eacute;nom : 	<b>$civilite $nom $prenom</b><br>
NTelephone :  <b>$tel</b><br>
NPortable:  <b>$mobile</b><br>
Email :  <b>$email</b> <br><br>
 

Objet : 	<b>$objet</b><br> 
Commentaires : <b>$commentaire</b> <br>
 
</body></html>";	 
/*
      if ( (preg_match("#[0][6][- \.?]?([0-9][0-9][- \.?]?){4}$#", $tel)) || (preg_match("#[0][7][- \.?]?([0-9][0-9][- \.?]?){4}$#", $tel)) || (preg_match("#[0][1][- \.?]?([0-9][0-9][- \.?]?){4}$#", $tel)))
		  {
			  */
$r1 = substr($tel,0,1);			  
if  ($r1 != 8 )
{	  
mail("technique@proxiweb.tn",'Formulaire Contact PROXIWEB',$html,$headers);
mail("info@proxiweb.tn",'Formulaire Contact PROXIWEB',$html,$headers);
mysqli_query($GLOBALS['con'],"INSERT INTO `dev_form_contact` (   `id_contact`, `id_site`,`civilite` , `nom` , `prenom` , `email` , `tel` , `mobile` , `commentaires` , `ip_user`, `date` )
	VALUES (  '','0','$civilite' ,'$nom', '$prenom', '$email', '$tel', '$mobile', '$objet $commentaire  proxiweb.tn', '$_SERVER[REMOTE_ADDR]', NOW())");
 }
 
 /* } */
	
	
}	
}
else
{	
?>
                            <div class="contact_form commentform">
                                <form id="contactform" class="row" action="" name="contactform" method="post">
                                    <div class="col-md-6 col-sm-12">
                                        <label>Nom <span class="required">*</span></label>
                                        <input type="text" name="nom" id="nom" class="form-control" placeholder=""> 
                                    </div>
									
									   <div class="col-md-6 col-sm-12">
                                        <label>Prénom <span class="required">*</span></label>
                                        <input type="text" name="prenom" id="prenom" class="form-control" placeholder=""> 
                                    </div>
									
									
                                    <div class="col-md-6 col-sm-12">
                                        <label>Email <span class="required">*</span></label>
                                        <input type="text" name="email" id="email" class="form-control" placeholder=""> 
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <label>Téléphone Mobile <span class="required">*</span></label>
                                        <input type="text" name="tel" id="tel" class="form-control" placeholder=""> 
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <label>Commentaire <span class="required">*</span></label>
                                        <textarea class="form-control" name="commentaire" id="commentaire" rows="6" placeholder="Votre Message..."></textarea>
                                    </div>
									
									
									    <div class="col-md-12 col-sm-12">
                                     <script src='https://www.google.com/recaptcha/api.js'></script>
<div class="g-recaptcha" data-sitekey="6LfhqfEUAAAAALoUV8xbDMZaHyETSEM5KJacl7Vo"></div> 
                           </div>
									
                                    <div class="col-md-12 col-sm-12">
                                        <button type="submit" value="Envoyer" id="Envoyer"  name="Envoyer" class="btn btn-primary"> Envoyer</button>
                                    </div>
                                </form>
                            </div><!-- end commentform -->
							
<?php
}
?>


                        </div><!-- end wbp -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->



        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="support-box greybg">
                            <h3>Département commercial</h3>
                            <p>Appelez le 71 891 521 Pour les questions de pré-vente, nous vous transmettons vers le service approprié.</p>

                        </div><!-- end support-box -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="support-box bluebg">
                            <h3>Support en ligne</h3>
                            <p>Vous pouvez obtenir de l'aide en contactant notre assistance en direct via l'opérateur actif sur la page en bas à droite située.</p>

                        </div><!-- end support-box -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="support-box greenbg">
                            <h3>Département Technique</h3>
                            <p>Vous pouvez nous envoyer un mail sur info@proxiweb.tn ou Appelez le 71 891 521</p>


                        </div><!-- end support-box -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="support-box purplebg">
                            <h3>ADRESSE </h3>
                            <p>Immeuble PROXIWEB<br>41 Avenue Hedi Chaker Lafayette TUNIS<br>
                            <a href="#">info@proxiweb.tn</a></p>
                        </div><!-- end support-box -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
 


<?php 
include ("footer.php");

?>
 