          <section class="smallsec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <h3>Vous avez une question avant de commander ?</h3>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="contact.php" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Contacter nous</a>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
   <footer class="footer lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Nos services</h4>
                            </div><!-- end widget-title -->

                            <div class="link-widget">   
                                <ul class="check">
                                    <li><a href="service-creation-site-internet.php">Création sites internet</a></li>
                                    <li><a href="service-hebergement-web.php">Hébergement Web</a></li>
                                    <li><a href="service-referencement-seo.php">Référencement et SEO</a></li>
                                        <li><a href="service-marketing-sea.php">Marketing SEA</a></li>
                                    <li><a href="service-email-pro.php">Emails Pro </a></li>
                                   <li><a href="service-community-manager.php">Cmmunity manager</a></li>
                                </ul><!-- end check -->
                            </div><!-- end link-widget -->
                        </div>

                        <hr>

                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Noms Domaines</h4>
                            </div><!-- end widget-title -->

                            <div class="link-widget">   
                                <ul class="check">
                                  <li><a href="#">Acheter un Domaine</a></li>
                                    <li><a href="#">Nom Domaine Pro</a></li>
                                    <li><a href="#">Transferer Domaine</a></li>
                                    <li><a href="#">Réserver Domaine  </a></li>
                                </ul><!-- end check -->
                            </div><!-- end link-widget -->
                        </div>
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>PROXIWEB</h4>
                            </div><!-- end widget-title -->

                            <div class="link-widget">   
                                <ul class="check">
                                    <li><a href="#">À propos de PROXIWEB</a></li>
                                    <li><a href="contact.php">Contact & infos</a></li>
                                    <li><a href="#">Nos équipes</a></li>
                                    <li><a href="#">Service développement</a></li>
                                    <li><a href="#">Service commercial</a></li>
                                    <li><a href="#">Récompenses et Avis</a></li>
                                    <li><a href="#">PROXIWEB dans la presse</a></li>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Documentations et Guides</a></li>
                                    <li><a href="#">Knowledgebase</a></li>
                                    <li><a href="#">Affiliation & Partenariat</a></li>
								    <li><a href="cgv.php">CGV</a></li>
									<li> <a href="mentions-legales.php">Mentions légales</a></li> 
                                </ul><!-- end check -->
                            </div><!-- end link-widget -->
                        </div>
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-12">
                        <div class="widget clearfix">
						
						    <iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/proxiweb.tn/&amp;width=300&amp;height=395&amp;show_faces=false&amp;colorscheme=light&amp;stream=true&amp;border_color&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:395px; background-color:#ffffff;" allowtransparency="true"></iframe>
                       
						<!--
                            <div class="widget-title">
                                <h4>Support</h4>
                            </div> 

                            <div class="link-widget">   
                                <ul class="check">
                                    <li><a href="#">Chat en ligne</a></li>
                                    <li><a href="#">Documentations</a></li>
                                    <li><a href="#">Support par Ticket</a></li>
                                    <li><a href="#">Tutorials Videos </a></li>
                                    <li><a href="contact.php">Contactez nous</a></li>
                                </ul> 
                            </div> 
                        </div>

                        <hr>

                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Noms de domaines</h4>
                            </div> 

                            <div class="link-widget">   
                                <ul class="check">
                                    <li><a href="#">Acheter un Domaine</a></li>
                                    <li><a href="#">Nom Domaine Premium</a></li>
                                    <li><a href="#">Transferer un Domaine</a></li>
                                    <li><a href="#">Réserver un Domaine  </a></li>
                                    <li><a href="#">Gérer Domaines</a></li>
                                </ul> 
                            </div> 
							-->
                        </div>
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-12">
                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Email Newsletter</h4>
                            </div><!-- end widget-title -->

                            <div class="newsletter-widget">
                                <p>Abonnez-vous à notre newsletter pour les codes de réduction et de réduction</p>
                                <form>
                                    <input type="text" class="form-control input-lg" placeholder="Votre nom" />
                                    <input type="email" class="form-control input-lg" placeholder="Email" />
                                    <button class="btn btn-primary btn-block">Abonnez-vous maintenant</button>
                                </form>
                            </div><!-- end newsletter -->
                        </div>

                        <hr>

                        <div class="widget clearfix">
                            <div class="widget-title">
                                <h4>Nous trouver</h4>
                            </div><!-- end widget-title -->
                            <div class="downloadbuttons clearfix">
                                <a href="#">
                                    

<picture>
<source srcset="images/appstore.webp" type="image/webp">
<source srcset="images/appstore.png" type="image/jpg"> 
<img src="images/appstore.png" alt="">
</picture>

                                </a>
                                <a href="#">
                                <picture>
<source srcset="images/googlestore.webp" type="image/webp">
<source srcset="images/googlestore.png" type="image/jpg"> 
<img src="images/googlestore.png" alt="">
</picture>
                                </a>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </footer><!-- end footer -->

        <div class="footer-distributed">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 footer-left">
                        <div class="widget">
                        <picture>
<source srcset="images/proxiweb.webp" type="image/webp">
<source srcset="images/proxiweb.jpg" type="image/jpg"> 
<img src="images/proxiweb.jpg" alt="">
</picture>
                        <p class="footer-links">
                            <a href="/">Accueil</a>
                            ·
                            <a href="#">À propos de PROXIWEB</a>
                            ·
                            <a href="tarifs.php">Tarifs</a>
                            ·
                            <a href="mentions-legales.php">Mentions légales</a>
                            ·
                            <a href="cgv.php">CGV</a>
                            ·
                            <a href="contact.php">Contact</a>
                        </p>
                        <p class="footer-company-name">PROXIWEB &copy; 2017 - <?php echo date('Y'); ?></p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 footer-center">
                        <div class="widget">
                        <div>
                            <i class="fa fa-map-marker"></i>
                            <p>Immeuble PROXIWEB<br>41 Avenue Hedi Chaker Lafayette TUNIS</p>
                        </div>
                        <div>
                            <i class="fa fa-phone"></i>
                            <p><a href="tel:31 320 300">31 320 300</a></p>
                        </div>
                        <div>
                            <i class="fa fa-envelope-o"></i>
                            <p><a href="mailto:info@proxiweb.tn">info@proxiweb.tn</a></p>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12 footer-right">
                        <div class="widget">
                        <p class="footer-company-about">
                            <span>À propos de </span>
                          PROXIWEB est le partenaire de choix des petites et moyennes entreprises pour les solutions d'hébergement Web et les infrastructures Cloud. 
						  Nous offrons un portefeuille de produits et de services-clés qui permettent aux entreprises de démarrer et de développer leur activité sur Internet

                        </p>
                        <div class="footer-icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-github"></i></a>
                        </div>
                        </div>
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end copyrights -->
<!--
        <div class="chat-wrapper">
            <div class="panel panel-primary">
                <div class="panel-heading" id="accordion">
                    <a class="btn btn-primary btn-block btn-xs" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <span class="fa fa-comments-o"></span> Support en ligne
                    </a>
                </div>
                <div class="panel-collapse collapse" id="collapseOne">
                    <div class="panel-body">
                        <ul class="chat">
                            <li class="left clearfix">
                                <span class="chat-img">
                                    <img src="upload/client_01.png" alt="User Avatar" class="img-circle img-responsive alignleft" />
                                </span>
                                <div class="chat-body clearfix">
                                    <div class="chat-header">
                                        <strong class="primary-font">John DOE</strong> <small class="pull-right text-muted">
                                        <span class="fa fa-clock-o"></span>12 mins ago</small>
                                    </div>
                                    <p>Hello anyone here? I need to purchase web hosting!</p>
                                </div>
                            </li>
                      
                            <li class="left clearfix">
                                <span class="chat-img">
                                    <img src="upload/client_01.png" alt="User Avatar" class="img-circle img-responsive alignleft" />
                                </span>
                                <div class="chat-body clearfix">
                                    <div class="chat-header">
                                        <strong class="primary-font supportstaff">Staff</strong> <small class="pull-right text-muted">
                                        <span class="fa fa-clock-o"></span>13 mins ago</small>
                                    </div>
                                    <p>Hey John! Welcome to the HostHubs support chat!</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <div class="input-group">
                            <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" id="btn-chat">Envoyer</button>
                            </span>
                        </div>
                    </div><!-- end panel-footer -->
                </div><!-- end panel-collapse -->
            </div><!-- end panel -->
        </div><!-- end chat-wrapper -->

    <div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->

    <!-- Main Scripts-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>

  
<!--
<script src="https://proxiweb.tn/js/cookiechoices.js"></script>
<script>document.addEventListener('DOMContentLoaded', function(event){cookieChoices.showCookieConsentBar('Ce site utilise des cookies pour vous offrir le meilleur service. En poursuivant votre navigation, vous acceptez l’utilisation des cookies.', 'J’accepte', 'En savoir plus', 'https://proxiweb.tn/mentions-legales');});</script>
!-->
<!--	
<script type="text/javascript">
var LHCChatOptions = {};
LHCChatOptions.opt = {widget_height:340,widget_width:300,popup_height:520,popup_width:500,domain:'proxiweb.tn'};
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
po.src = '//proxiweb.tn/livechat/index.php/fre/chat/getstatus/(click)/internal/(position)/bottom_right/(hide_offline)/true/(top)/350/(units)/pixels/(leaveamessage)/true/(department)/2?r='+referrer+'&l='+location;
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>
 !-->
 
 <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/fr_FR/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="1557609490942713"
  logged_in_greeting=" Salut! Comment pouvons-nous vous aider?"
  logged_out_greeting=" Salut! Comment pouvons-nous vous aider?">
      </div>

</body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-164490110-2', 'proxiweb.tn');
  ga('send', 'pageview');

</script>



</html>