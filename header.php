﻿<?php
session_start();
ini_set('display_errors', 1);  
header("Expires: ".gmdate("D, d M Y H:i:s", time() + 604800)." GMT");
header("Cache-Control: public, max-age=604800");
require('include/connexion.php'); 
  if(isset($_REQUEST["deconnect"])){
	   unset($_SESSION[accountclient]); 
	   unset($_SESSION[id_client]); 
  
  }
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="fr"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="fr"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="fr"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="fr"> <!--<![endif]-->
 
<head>
<meta name="google-site-verification" content="hh-StBKXks3pjluxKZZjqyitKzU5YAL9KpQsU3DAJQI" />
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="images/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/apple-touch-icon-152x152.png">

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">

    <!-- BOOTSTRAP STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <!-- TEMPLATE STYLES -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- RESPONSIVE STYLES -->
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <!-- CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">

    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>

	<!-- PRELOADER  
        <div id="loader">
			<div class="loader-container">
				<img src="images/load.gif" alt="" class="loader-site spinner">
			</div>
		</div>
	<!-- END PRELOADER -->

    <div id="wrapper">
        <div class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 text-left cenmobile">
                        <div class="topmenu">
                            <span><i class="fa fa-envelope-o"></i> <a href="devis-creation-projet-web.php">Devis Création Site</a></span>  
<span  ><i class="fa fa-money"></i> <a href="simulateur-tarif-projet-web.php">Simulateur Tarifs</a></span>  

<span  ><i class="fa fa-money"></i> <a href="tarifs.php">Tarifs</a></span>  
                 <span  ><i class="fa fa-file"></i> <a href="contact.php">Devenir Revendeur</a></span>  
					   </div><!-- end callus -->
                    </div>

                    <div class="col-md-6 col-sm-12 text-right cenmobile">
                        <div class="topmenu rightv">
						    
    <style> 
.btnx2-primaryx2 {
    color: #fff !important;
    background-color: #16a974 !important;
    border-color: #16a974 !important;
    box-shadow: 0 0.3125rem 1.25rem rgba(0,0,0,.05);
	width:200px  !important;
	font-size:20px  !important;
}
.btnx2 {
    display: inline-block;
    font-weight: 600;
    color: #7c8085;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: .0625rem solid transparent;
    padding: 1rem 2rem;
    font-size: 1rem;
    line-height: 1.7;
    border-radius: .3rem;
    -webkit-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
 
 .btnx3-primaryx3 {
    color: #fff !important;
    background-color: #13384A !important;
    border-color: #13384A !important;
    box-shadow: 0 0.3125rem 1.25rem rgba(0,0,0,.05);
	width:200px  !important;
	font-size:20px  !important;
}
.btnx3 {
    display: inline-block;
    font-weight: 600;
    color: #7c8085;
    text-align: center;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-color: transparent;
    border: .0625rem solid transparent;
    padding: 1rem 2rem;
    font-size: 1rem;
    line-height: 1.7;
    border-radius: .3rem;
    -webkit-transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
</style>			<!--
                            <div class="dropdown">
                                <a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> TND <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="#">EUR</a></li> 
									 <li><a href="#">USD</a></li> 
                                </ul>
                            </div>
							!-->
							
		   <span><a data-placement="bottom" data-toggle="tooltip" title="Français" href="#"  class="flag_link fre" data-lang="fr" >
               

<picture>
<source srcset="images/FR.webp" type="image/webp">
<source srcset="images/FR.png" type="image/jpg"> 
<img src="images/FR.png" alt="">
</picture>

            </a></span>
                           <span><a data-placement="bottom" data-toggle="tooltip" title="Arabe" href="#"  class="flag_link deu" data-lang="ar">
                           <picture>
<source srcset="images/AR.webp" type="image/webp">
<source srcset="images/AR.png" type="image/jpg"> 
<img src="images/AR.png" alt="">
</picture>    
                            </a></span>
		 		 		    
                            <span><a data-placement="bottom" data-toggle="tooltip" title="Anglais" href="#"  class="flag_link eng" data-lang="en" >
                            <picture>
<source srcset="images/AU.webp" type="image/webp">
<source srcset="images/AU.png" type="image/jpg"> 
<img src="images/AU.png" alt="">
</picture>     
                            </a></span>
                         
<div id="google_translate_element"></div>
<script type="text/javascript">
   function googleTranslateElementInit() {
       new google.translate.TranslateElement({ pageLanguage: 'fr' }, 'google_translate_element');
   }
</script>
<script type="text/javascript"
   src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
 
 
<style>
#google_translate_element{
  display: none;
}
.goog-te-banner-frame.skiptranslate {
        display: none !important;
    } 
    body {
        top: 0px !important; 
    }
 </style>
<script type="text/javascript">
var flags = document.getElementsByClassName('flag_link');


Array.prototype.forEach.call(flags, function(e){
  e.addEventListener('click', function(){
    var lang = e.getAttribute('data-lang'); 
    var languageSelect = document.querySelector("select.goog-te-combo");
    languageSelect.value = lang; 
    languageSelect.dispatchEvent(new Event("change"));
  }); 
});
</script>




<?php  
    $scriptname=$_SERVER['SCRIPT_NAME'];
$scriptname=str_replace("/",'',$scriptname);
$scriptname=str_replace(".php",'',$scriptname); 
$scriptname=str_replace("-",' ',$scriptname);
$scriptname=str_replace("_",' ',$scriptname);
 
  if (strstr($scriptname, 'bouygues'))
	  {
?>
    <a href="tel:+216 31 320 300" onclick="ga('send', 'event', 'Phone Call Tracking (+216) 31 320 300', 'click-to-call', 'header');" class="btnx2 btnx2-primaryx2">
<i class="fa fa-phone"></i>31 320 300 



<picture>
<source srcset="images/AR.webp" type="image/webp">
<source srcset="images/AR.png" type="image/jpg"> 
<img src="images/AR.png" alt="" align="absmiddle" style="vertical-align:absmiddle">
</picture>

</a>
<?php	  
	  
	  }
	   else 
	  {
?>
   <!-- <a href="tel:71 891 521" onclick="ga('send', 'event', 'Phone Call Tracking 71 891 521', 'click-to-call', 'header');" class="btnx2 btnx2-primaryx2">
<i class="fa fa-phone"></i> 71 891 521 <img src="images/FR.png" alt="" align="absmiddle" style="vertical-align:absmiddle">
</a>
!-->
    <a href="tel:+216 31 320 300" onclick="ga('send', 'event', 'Phone Call Tracking (+216) 31 320 300', 'click-to-call', 'header');" class="btnx2 btnx2-primaryx2">
<i class="fa fa-phone"></i>31 320 300 


<picture>
<source srcset="images/AR.webp" type="image/webp">
<source srcset="images/AR.png" type="image/jpg"> 
<img src="images/AR.png" alt="" align="absmiddle" style="vertical-align:absmiddle">
</picture>


</a>
<?php	  
	  
	  }
?>

    	<a href="contact.php" class="btnx3 btnx3-primaryx3"><i class="fa fa-comments-o"></i> Contactez nous</a>
	
			


                        </div><!-- end callus -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end topbar -->
        </div><!-- end topbar -->

        <header class="header">
            <div class="">
                <nav class="navbar navbar-default yamm">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.php">
                                

<picture>
<source srcset="images/proxiweb.webp" type="image/webp">
<source srcset="images/proxiweb.jpg" type="image/jpg"> 
<img  src="images/proxiweb.jpg" alt="PROXIWEB" title="PROXIWEB">
</picture>

                            </a>
                        </div>

                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                     
                                <li class="dropdown has-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sites Internet <span class="fa fa-angle-down"></span> <span class="label label-success hidden-xs">Offre</span></a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown"><a href="modele.php"  >Nos modèles</a>
                                       
                                        </li>
                                        <li class="dropdown"><a href="modele.php"  >Boutique en ligne  </span></a>
                                       
                                        </li>
                                        <li class="dropdown"><a href="modele.php" >Site sur mesure  </a>
                                     
                                        </li>
                                        <li class="dropdown"><a href="nos-realisations.php"  >Nos Réalisations  </a>
                                    
                                        </li>
										<!--
                                        <li><a href="page-about.html">About us</a></li>
                                        <li><a href="page-services.html">Services</a></li>
                                        <li><a href="page-team.html">Team Members</a></li>
                                        <li><a href="page-clients.html">Testimonials & Clients</a></li>
                                        <li><a href="page-contact.html">Contact us</a></li>
										!-->
                                    </ul>
                                </li>
								
								
								 
								
								
								
								
                                <li class="megamenu dropdown hasmenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="fa fa-angle-down"></span></a>
                                    <ul class="dropdown-menu withbgcolor">
                                        <li>
                                            <div class="yamm-content clearfix">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                                        <div class="widget clearfix">
                                                            <div class="big-title">
                                                                <h3>Nos Packs Création Web<br>
                                                                <span>FONCTIONALITÉS</span>
                                                                </h3>
                                                            </div><!-- end big-title -->

                                                            <div class="email-widget">
                                                                <p>Ci-dessous nos super offres Création Web . Vous pouvez sélectionner n'importe quel service Web ci-dessous!</p>
                                                                <ul class="check-list">
                                                                    <li>SITE PRÉSENCE PROXIWEB-Basic</li>
                                                                    <li>SITE PRÉSENCE PROXIWEB-Premium</li>
                                                                    <li>SITE PROXIWEB-SURMESURE</li>
                                                                    <li>SITE PROXIWEB-ECOMMERCE</li> 
                                                                </ul><!-- end check -->
                                                            </div><!-- end email widget -->
                                                        </div>
                                                    </div><!-- end col -->
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="widget clearfix">
                                                            <div class="widget-title">
                                                                <h4>Nos services</h4>
                                                            </div><!-- end widget-title -->

                                                            <div class="link-widget">   
                                                                <ul>
                                     <li><a href="service-creation-site-internet.php">Création sites internet</a></li>
                                    <li><a href="service-hebergement-web.php">Hébergement Web</a></li>
                                    <li><a href="service-referencement-seo.php">Référencement SEO</a></li>
                                    <li><a href="service-marketing-sea.php">Marketing SEA</a></li>
                                    <li><a href="service-email-pro.php">Emails Pro </a></li>
                                    <li><a href="service-community-manager.php">Community Manager</a></li> 
                                    <li><a href="service-nom-domaine.php">Nom Domaine Premium</a></li>
                                                                </ul><!-- end check -->
                                                            </div><!-- end link-widget -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="widget clearfix">
                                                            <div class="widget-title">
                                                                <h4>PROXIWEB</h4>
                                                            </div><!-- end widget-title -->

                                                            <div class="link-widget">   
                                                                <ul>
                                                           <li><a href="#">À propos de PROXIWEB</a></li>
                                    <li><a href="contact.php">Contact & infos</a></li>
                                    <li><a href="#">Nos équipes</a></li>
                                    <li><a href="#">Service développement</a></li>
                                    <li><a href="#">Service commercial</a></li>
                                    <li><a href="#">Récompenses et Avis</a></li>
                                    <li><a href="#">PROXIWEB dans la presse</a></li>
                                      <li><a href="#">Affiliation & Partenariat</a></li>
                                                                </ul><!-- end check -->
                                                            </div><!-- end link-widget -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-sm-12 col-xs-12">
                                                        <div class="widget clearfix">
                                                            <div class="widget-title">
                                                                <h4>Contact & Support</h4>
                                                            </div><!-- end widget-title -->

                                                            <div class="link-widget">   
                                                                <ul>
                                  
                                    <li><a href="#">Documentations et Guides</a></li>
                                    <li><a href="#">Knowledgebase</a></li>
									 <li><a href="#">Chat en ligne</a></li>
                                    <li><a href="#">Documentations</a></li>
									    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Support par Ticket</a></li>
                                    <li><a href="#">Tutorials Videos </a></li>
                                    <li><a href="contact.php">Contactez nous</a></li>
                                                                </ul><!-- end check -->
                                                            </div><!-- end link-widget -->
                                                        </div><!-- end widget -->
                                                    </div>
                                                </div><!-- end row -->
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                             
                            
							  
							
							 <li class="dropdown has-submenu">
                                    <a href="https://seo.proxiweb.tn/"  >Marketing<!--<span class="fa fa-angle-down"></span>--> </a>
                                 <!--   <ul class="dropdown-menu">
                                        <li ><a href="marketing-referencement-naturel.php" >Référencement naturel</a> </li>
										<li ><a href="marketing-google-business.php" >Google Business </a> </li>
										 <li ><a href="marketing-google-ads.php" >Google Ads </a> </li>
										  <li ><a href="marketing-facebook-ads.php" >Facebook Ads </a> </li>
										   <li ><a href="marketing-pub-email.php" >PUB Email </a> </li>
										    <li ><a href="marketing-pub-sms.php" >PUB SMS</a> </li>
							          </ul>-->
                                </li>
							
							
                               <li class="megamenu dropdown hasmenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TOPNET <span class="fa fa-angle-down"></span> <span class="label label-success2 hidden-xs">ADSL</span></a>
                                    <ul class="dropdown-menu withbgcolor">
                                        <li>
                                            <div class="yamm-content clearfix">
                                                <div class="row">
                                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                                        <div class="widget clearfix">
                                                            <div class="big-title">
                                                                <h3>TOPNET ADSL / VDSL / FIBRE OPTIQUE  </h3><br>
                                                                <span>
                                                                

<picture>
<source srcset="images/topnet.webp" type="image/webp">
<source srcset="images/topnet.pjpgng" type="image/jpg"> 
<img src="images/topnet.jpg"   alt="TOPNET" title="TOPNET" class="img-responsive" width="80%" ></span>
</picture>

                                                               
                                                            </div><!-- end big-title -->
 
                                                        </div>
                                                    </div><!-- end col -->
                                                    
                                                   
                                                    <div class="col-md-2 col-sm-12 col-xs-12">
                                                        <div class="widget clearfix">
                                                            <div class="widget-title">
                                                                <h4>TOPNET PROMOS</h4>
                                                            </div><!-- end widget-title -->
 
                                                            <div class="link-widget">   
                                                                <ul>
                                                                    <li><a href="topnet.php">SMART ADSL</a></li>
																	  <li><a href="topnet.php">ADSL RELAX</a></li>
                                                                    <li><a href="topnet.php">VDSL TOPNET</a></li>
                                                                    <li><a href="topnet.php">FIBRE OPTIQUE</a></li> 
                                                                <li><a href="topnet.php">PAYEMENT FACTURES</a></li> 
																</ul><!-- end check -->
                                                            </div><!-- end link-widget -->
                                                        </div><!-- end widget -->
                                                    </div>
                                                </div><!-- end row -->
                                            </div>
                                        </li>
                                    </ul>
                                </li>
								
								      <li class="dropdown has-submenu">
                                    <a href="https://shop.proxiweb.tn" class="dropdown-toggle"   role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart"></i>PROXIWEB<span class="label label-success hidden-xs">Shop</span></a>
                          </li>
							
			      <li class="dropdown has-submenu">
                                    <a href="https://hosting.proxiweb.tn" class="dropdown-toggle"   role="button" aria-haspopup="true" aria-expanded="false">Hébergement & nom de domaine<span class="label label-success2 hidden-xs">New</span></a>
                          </li>

 						
                            </ul>


    <?php
  /*
if ($_SESSION["accountclient"]!='')  
{
	
  ?>
  
  
               <ul class="nav navbar-nav navbar-right hidden-xs">
                                <li class="dropdown searchmenu hasmenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Mon Compte [<?php echo $_SESSION["accountclient"] ; ?>] <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu show-right">
                                        <li><a href='?deconnect'>Déconnexion</a>   </li>
                                    </ul>
                                </li>
                            </ul>
  
  
  
  <?php
}
else
{ 
  ?>
             <ul class="nav navbar-nav navbar-right hidden-xs">
                                <li class="dropdown searchmenu hasmenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Connexion <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu show-right">
                                        <li>
                                            <div id="custom-search-input">
                                                <div class="input-group col-md-12">
												   <form action="compte.php" method="post"> 
												   
												   
												   
                                                    <input type="text" class="form-control input-lg" name="accountlogin" placeholder="Login" value="" />
                                                    <input type="password" class="form-control input-lg" name="passwordlogin" id="password2" placeholder="Mot de passe"  value="" />
                                                    <label><a href="compte.php?motdepasse">Mot de passe oublié ?</a></label>
                                                    <button type="submit" id="_submit" name="submit" value="Se connecter" class="btn btn-primary btn-block">Se connecter</button>
                                                
												 </form>
												 </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>            
  <?php
} 
*/
?>	


               
							
							
							
							
							
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </nav><!-- end nav -->
            </div><!-- end container -->
        </header><!-- end header -->

        <div class="after-header">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <ul class="list-inline text-center">
<?php
 if (strstr($scriptname, 'contact')) $monmenuactive1='class="active"' ; else $monmenuactive1='' ; 
  if (strstr($scriptname, 'modele')) $monmenuactive2='class="active"' ; else $monmenuactive2='' ; 
   if (strstr($scriptname, 'realisations')) $monmenuactive3='class="active"' ; else $monmenuactive3='' ; 
    if (strstr($scriptname, 'compte')) $monmenuactive4='class="active"' ; else $monmenuactive4='' ; 
?>
						    <li <?php echo $monmenuactive1 ; ?>><a href="https://hosting.proxiweb.tn">Acheter Nom domaine</a></li>
   <li <?php echo $monmenuactive1 ; ?>><a href="https://hosting.proxiweb.tn">Commander Hébergement</a></li>
                                                      
						   <li <?php echo $monmenuactive2 ; ?>><a href="modele.php">Modèles Sites</a></li>
                            <li <?php echo $monmenuactive3 ; ?>><a href="nos-realisations.php">Nos créations</a></li> 
							
							 
							    <?php
  
if ($_SESSION["accountclient"]!='')  
{
	
  ?>
      <li <?php echo $monmenuactive4 ; ?> ><a href='compte.php'>Mon Compte [<?php echo $_SESSION["accountclient"] ; ?>]</a></li> 
                            <li> <a href='?deconnect'>Déconnexion</a></li> 
  <?php
}
else
{ 
  ?>
 				<li <?php echo $monmenuactive4 ; ?> ><a href="compte.php" >Mon compte</a></li>
               
  <?php
} 
?>	
							
                           
                           
                        </ul>
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end after-header -->
