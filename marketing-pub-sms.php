<?php
include ("header.php");
?>

  <title>Marketing - Plus de clients avec PUB SMS - PROXIWEB</title>
    <meta name="description" content="Lancez des PUB SMS en quelques minutes. Exécutez-les comme les plus grandes marques de l&#039;industrie. PROXIWEB automatisera tout, de la création à l&#039;optimisation, et vous aidera à atteindre vos objectifs mieux que quiconque.">
  
          


   <section class="section paralbackground page-banner" style="background-image:url('upload/page_banner_05.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title lb">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>PUB SMS<small>Plus de clients avec PUB SMS</small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="/">Accueil</a></li>
                            <li class="active">PUB SMS</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="affbox">
                            <h3>Définissez votre client cible</h3>
                            <h4>Choisissez votre objectif Pub SMS</h4>
                        </div><!-- end affilitebox -->

                        <div class="greybox">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="wb">
                                        <div class="big-title">
                                            <h3>Vous cherchez un spécialiste de PUB SMS ?<br>
                                            <span>Plus de clients avec Pub SMS</span>
                                            </h3>
                                        </div><!-- end big-title -->

                                        <div class="email-widget">
                                            <p>
											PROXIWEB crée des publicités Pub SMS attrayantes et les optimise automatiquement pour vous permettre d'atteindre vos objectifs commerciaux.
											<br>
											 
											</p>
											<br><br>
											La création de vos campagnes PUB SMS est incluse dans nos pack création siteweb .
											
											
                                            <ul class="check-list">
                                                <li>PUB SMS inclus dans nos offres siteweb</li>
                                                <li>PUB SMS est à 120 DT / an dans notre offre marketing</li>
                                             <li>Offre Marketing inclus ( Google ads , PUB SMS ...) avec suivi et optimisation</li>
                                            </ul><!-- end check -->

                                            <a href="commander.php" class="btn btn-default">Offre siteweb</a> <a href="contact?&prestation=Marketing&leprix=120" class="btn btn-primary">Offre marketing</a>

                                        </div><!-- end email widget -->
                                    </div><!-- end wb -->
                                </div><!-- end col -->

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                <picture>
<source srcset="images/marketing-pub-sms.webp" type="image/webp">
<source srcset="images/marketing-pub-sms.jpg" type="image/jpg"> 
<img src="images/marketing-pub-sms.jpg" alt="" class="img-responsive">
</picture>
                                </div><!-- end col -->
                            </div><!-- end row -->

                            <hr>

                        
                        </div><!-- end greybox -->

                        <div class="affbox">
                            <h3>Pourquoi Annoncer Sur Pub SMS?</h3>
                            <h4>Diffusez des annonces uniquement lorsque les utilisateurs recherchent votre produit ou service.</h4>
                        </div><!-- end affilitebox -->

                    </div><!-- end affbox -->
                </div><!-- end col -->
            </div><!-- end container -->
        </section><!-- end section -->


   <section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Pourquoi Annoncer Sur <span>Pub SMS?</span></h3>
 
                </div><!-- end section-title -->

                <div class="row services-list hover-services text-center">
                    <div class="col-md-4 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-users"></i>
                            <h3> </h3>
                            <p>Plus de 2,4 milliards d'utilisateurs mensuels actifs</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-4 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-smile-o"></i>
                            <h3></h3>
                            <p>Le meilleur endroit pour créer une communauté et trouver de nouveaux fans</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-4 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-dollar"></i>
                            <h3></h3>
                            <p>Faites des publicités attrayantes avec des images ou des vidéos</p>
                        </div><!-- end box -->
                    </div><!-- end col -->
                </div>
            </div><!-- end container -->
        </section><!-- end section -->




 


<?php 
include ("footer.php");

?>
 