<?php
include ("header.php");
?>

  <title>Mentions légales - PROXIWEB   </title>
    <meta name="description" content="Mentions légales PROXIWEB vous apporte les solutions les plus adaptées à vos besoins et à votre budget pour être visible et lisible sur Internet et mieux développer votre activité. Nous offrons une gamme complète d’expertise en transformation digitale.">
  
          


 

        <section class="section paralbackground page-banner" style="background-image:url('upload/page_banner_01.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title lb">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Mentions légales  <small>Pour plus infos Centre Support </small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="/">Accueil</a></li>
                            <li class="active">Mentions légales </li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->

  

        <section class="section lb">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Email Support</h3>
                                <hr>
                            </div><!-- end big-title -->

                            <div class="email-widget">
                                <ul class="check-list">
                                    <li><a href="#">info@proxiweb.tn</a></li> 
                                </ul><!-- end check -->
                            </div><!-- end email widget -->
                        </div><!-- end wbp -->    
                    </div><!-- end col -->

                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="wbp">
                            <div class="small-title">
                                <h3>Mentions légales  </h3>
                                <hr>
                            </div><!-- end big-title -->

			
			 <style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	font-size:11.0pt;
	font-family:"Times New Roman",serif;}
@page WordSection1
	{size:595.0pt 842.05pt;
	margin:14.7pt 19.95pt 0cm 19.9pt;}
div.WordSection1
	{page:WordSection1;}
@page WordSection2
	{size:595.0pt 842.05pt;
	margin:14.7pt 19.95pt 0cm 19.9pt;}
div.WordSection2
	{page:WordSection2;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
 
  
 
<div class=WordSection2>



 <p style="text-align: justify;">L&rsquo;annonceur est l&rsquo;&eacute;diteur de ce site.</p>
<p style="text-align: justify;">PROXIWEB est le prestataire technique. <br />
<br /><strong>Raison sociale :</strong> PROXIWEB
<br /><strong>Adresse du siège social :</strong>Immeuble PROXIWEB<br>41 Avenue Hedi Chaker Lafayette TUNIS<br />
<strong>Adresse &eacute;lectronique :</strong> info@proxiweb.tn<br />
<strong>T&eacute;l&eacute;phone :</strong> 	71 891 521<br />
<strong>N&deg; SIRET de l&rsquo;&eacute;tablissement :</strong> MF: 1552671M/A/M/000 - RC : B0129122018<br />
<strong>Directeur de r&eacute;daction et de publication :</strong> PROXIWEB</p>


<p style="text-align: justify;"><strong>Webmaster</strong><br />www.proxiweb.tn<br />info@proxiweb.tn<br />H&eacute;bergeur<br />www.ovh.com</p>
<p style="text-align: justify;"><br /><strong>Informatique et libert&eacute;s</strong><br />Informations personnelles collect&eacute;es<br />En Tunisie, les donn&eacute;es personnelles sont notamment prot&eacute;g&eacute;es par la loi n&deg; 78-87 du 6 janvier 1978, la loi n&deg; 2004-801 du 6 ao&ucirc;t 2004, l&rsquo;article L. 226-13 du Code p&eacute;nal et la Directive Europ&eacute;enne du 24 octobre 1995. <br />En tout &eacute;tat de cause PROXIWEB ne collecte des informations personnelles relatives &agrave; l&rsquo;utilisateur (nom, adresse, adresse &eacute;lectronique, coordonn&eacute;es t&eacute;l&eacute;phoniques) que pour le besoin des services propos&eacute;s par le site, notamment pour la livraison de plats ou pour des traitements statistiques. L&rsquo;utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu&rsquo;il proc&egrave;de par lui-m&ecirc;me &agrave; leur saisie. Il est alors pr&eacute;cis&eacute; &agrave; l&rsquo;utilisateur du site le caract&egrave;re obligatoire ou non des informations &agrave; renseigner.</p>
<p style="text-align: justify;"><br /><strong>Autres informations</strong><br />Ce site utilise Google Analytics, un service d&rsquo;analyse de site internet fourni par Google Inc. (&laquo; Google &raquo;). Google Analytics utilise des cookies , qui sont des fichiers texte plac&eacute;s sur votre ordinateur, pour aider le site internet &agrave; analyser l&rsquo;utilisation du site par ses utilisateurs. Les donn&eacute;es g&eacute;n&eacute;r&eacute;es par les cookies concernant votre utilisation du site (y compris votre adresse IP) seront transmises et stock&eacute;es par Google sur des serveurs situ&eacute;s aux Etats-Unis. Google utilisera cette information dans le but d&rsquo;&eacute;valuer votre utilisation du site, de compiler des rapports sur l&rsquo;activit&eacute; du site &agrave; destination de son &eacute;diteur et de fournir d&rsquo;autres services relatifs &agrave; l&rsquo;activit&eacute; du site et &agrave; l&rsquo;utilisation d&rsquo;Internet. Google est susceptible de communiquer ces donn&eacute;es &agrave; des tiers en cas d&rsquo;obligation l&eacute;gale ou lorsque ces tiers traitent ces donn&eacute;es pour le compte de Google, y compris notamment l&rsquo;&eacute;diteur de ce site. Google ne recoupera pas votre adresse IP avec toute autre donn&eacute;e d&eacute;tenue par Google. Vous pouvez d&eacute;sactiver l&rsquo;utilisation de cookies en s&eacute;lectionnant les param&egrave;tres appropri&eacute;s de votre navigateur. Cependant, une telle d&eacute;sactivation pourrait emp&ecirc;cher l&rsquo;utilisation de certaines fonctionnalit&eacute;s de ce site. En utilisant ce site internet, vous consentez express&eacute;ment au traitement de vos donn&eacute;es nominatives par Google dans les conditions et pour les finalit&eacute;s d&eacute;crites ci-dessus.</p>
<p style="text-align: justify;"><br /><strong>Rectification des informations nominatives collect&eacute;es</strong><br />Conform&eacute;ment aux dispositions de l&rsquo;article 34 de la loi n&deg; 48-87 du 6 janvier 1978, l&rsquo;utilisateur dispose d&rsquo;un droit de modification des donn&eacute;es nominatives collect&eacute;es le concernant. Pour ce faire, l&rsquo;utilisateur envoie &agrave; <span class="thickbox" style="text-decoration: none;">PROXIWEB </span>:<br />un courrier &eacute;lectronique en utilisant le formulaire de contact du site<br />un courrier &agrave; l&rsquo;adresse du si&egrave;ge (indiqu&eacute;e ci-dessus) en indiquant son nom ou sa raison sociale, ses coordonn&eacute;es physiques et/ou &eacute;lectroniques, ainsi que le cas &eacute;ch&eacute;ant la r&eacute;f&eacute;rence dont il disposerait en tant qu&rsquo;utilisateur du site www.proxiweb.tn&nbsp; La modification interviendra dans des d&eacute;lais raisonnables &agrave; compter de la r&eacute;ception de la demande de l&rsquo;utilisateur.</p>
  



<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#B5B5B5'> 
Siège social PROXIWEB : Immeuble PROXIWEB<br>41 Avenue Hedi Chaker Lafayette TUNIS</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#B5B5B5'>Téléphone
: 71 891 521 - Email : info@proxiweb.tn - Site Web : www.proxiweb.tn</span></b></p>

<p class=MsoNormal align=center style='text-align:center'><b><span
style='font-size:12.0pt;font-family:"Arial",sans-serif;color:#B5B5B5'>MF: 1552671M/A/M/000 - RC : B0129122018</span></b></p>

</div>


                        </div><!-- end wbp -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->




 


<?php 
include ("footer.php");

?>
 