<?php
include ("header.php");
?>

  <title>Votre modèle web -PROXIWEB - Vue Responsive</title>
    <meta name="description" content="Vue Responsive est une aide à la conception Web utilisée pour afficher votre site Web réactif sur une variété de tailles d'écran.">


            <section class="section paralbackground page-banner" style="background-image:url('upload/page_banner_02.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

      
     
        <section class="section lb">
            <div class="container">
             
			 
<style>
#overlay {
	width:100vw;
	height:100vh;
	position:fixed;
	z-index:9999;
	top:0;
	bottom:0;
	background-color:#444;
	text-align:center;
}
#overlay i {
	margin-top:25%;
	color:#fff;
}
#overlay h1 {
	color:#fff;
}

iframe {
    background-color: #fff;
}


.helplink {
    border:1px solid #444;
    width:120px;
    text-align:center;
    margin:10px auto 50px;
    font-size:20px;
    font-weight:300;
    padding:5px;
	transition:all 200ms;
    background-color:#444;
    color:#fff;
}

.helplink:hover {
    background-color:#000;
	transition:all 200ms;
}

#topline {
    position:absolute;
    top:0;
    width:100%;
    height:10px;
    background-color:#444
}

#homepng {
    height:400px;
    width:700px;
    margin:20px auto 50px
}



#nav {
    
    overflow:visible;
    top:0;
    width:100%;
    height:40px;
    z-index:999;
    background-color:#444;
    color:#fff!important;
    font-weight:400!important;
}

#nav a {
    color:#fff;
    text-decoration:none;
    padding-left:30px;
    padding-right:30px
}

#nav ul {
    float:left;
    margin:0;
    padding:0
}

#nav ul li {
    float:left;
    line-height:40px;
    border-right:1px #666 solid
}

#nav ul li:hover {
    background-color:#333
}

ul {
    list-style:none
}

#navi {
    float:right
}

#navi ul {
    margin:auto
}

#navi ul li {
    width:175px;
    line-height:40px;
    height:40px;
    font-size:16px;
    text-align:center;
    float:left;
    color:#fff;
    border-left:1px #666 solid
}

#navi ul li:hover {
    background-color:#333
}

#navi ul li a {
    color:#fff;
    height:40px;
    display:block
}

#navi ul li a:hover {
    color:#fff
}

#navi ul li ul {
    width:175px
}

#navi ul li ul li {
    background-color:#fff;
    line-height:40px;
    float:left;
    display:none
}

#navi ul li ul li:last-child {
    border-bottom:1px #666 solid
}

#navi ul li ul li a {
    color:#000
}

#navi ul li ul li a:hover {
    color:#000
}

#navi ul li ul li:hover {
    background:#f0f0f0
}

#navi ul li:hover ul li {
    display:block
}

.pagelink {
    margin-top:3px;
    float:right;
    font-size:16px
}

/* iPhone 6 Plus*/
#iphone6pp {
    background-image:url(img/iphone6pp.png);
    height:770px;
    width:390px;
    margin:0 auto;
    margin-bottom:70px
}

.iphone6pps {
    height:723px;
    width:414px;
    margin:110px 33px;
    border:none;
    -ms-zoom:.78;
    -moz-transform:scale(0.78);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.78);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.78);
    -webkit-transform-origin:0 0
}

#iphone6pl {
    background-image:url(img/iphone6pl.png);
    height:390px;
    width:770px;
    margin:0 auto
}

.iphone6pls {
    height:390px;
    width:736px;
    margin:53px 98px;
    border:none;
    -ms-zoom:.78;
    -moz-transform:scale(0.78);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.78);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.78);
    -webkit-transform-origin:0 0
}

/* iPhone 6 */
#iphone6p {
    background-image:url(img/iphone6p.png);
    height:680px;
    width:340px;
    margin:0 auto;
    margin-bottom:60px
}

.iphone6ps {
    height:643px;
    width:375px;
    margin:105px 29px;
    border:none;
    -ms-zoom:.75;
    -moz-transform:scale(0.75);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.75);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.75);
    -webkit-transform-origin:0 0
}

#iphone6l {
    background-image:url(img/iphone6l.png);
    height:340px;
    width:680px;
    margin:0 auto
}

.iphone6ls {
    height:348px;
    width:667px;
    margin:50px 85px;
    border:none;
    -ms-zoom:.75;
    -moz-transform:scale(0.75);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.75);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.75);
    -webkit-transform-origin:0 0
}

/* iPhone 5 */
#iphone5p {
    background-image:url(img/iphone5p.png);
    height:660px;
    width:400px;
    margin:0 auto
}

.iphone5ps {
    height:548px;
    width:320px;
    margin:133px 80px;
    border:none;
    -ms-zoom:.75;
    -moz-transform:scale(0.75);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.75);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.75);
    -webkit-transform-origin:0 0
}

#iphone5l {
    background-image:url(img/iphone5l.png);
    height:750px;
    width:700px;
    margin:0 auto
}

.iphone5ls {
    height:300px;
    width:568px;
    margin:78px 137px;
    border:none;
    -ms-zoom:.75;
    -moz-transform:scale(0.75);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.75);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.75);
    -webkit-transform-origin:0 0
}

/* iPhone 4 */
#iphone4p {
    background-image:url(img/iphone4p.png);
    height:600px;
    width:520px;
    margin:0 auto
}

.iphone4ps {
    height:460px;
    width:320px;
    margin:135px 140px;
    border:none;
    -ms-zoom:.75;
    -moz-transform:scale(0.75);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.75);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.75);
    -webkit-transform-origin:0 0
}

#iphone4l {
    background-image:url(img/iphone4l.png);
    height:350px;
    width:600px;
    margin:0 auto
}

.iphone4ls {
    height:300px;
    width:480px;
    margin:70px 120px;
    border:none;
    -ms-zoom:.75;
    -moz-transform:scale(0.75);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.75);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.75);
    -webkit-transform-origin:0 0
}

/* iPad Pro */
#ipadpp {
    background-image:url(img/ipadpp.png);
    height:630px;
    width:540px;
    margin:0 auto
}

.ipadpps {
    height:1246px;
    width:1024px;
    margin:108px 85px;
    border:none;
    -ms-zoom:.36;
    -moz-transform:scale(0.36);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.36);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.36);
    -webkit-transform-origin:0 0
}

#ipadpl {
    background-image:url(img/ipadpl.png);
    height:450px;
    width:630px;
    margin:0 auto
}

.ipadpls {
    height:922px;
    width:1336px;
    margin:76px 74px;
    border:none;
    -ms-zoom:.362;
    -moz-transform:scale(0.362);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.362);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.362);
    -webkit-transform-origin:0 0
}

/* iPad Air */
#ipadairp {
    background-image:url(img/ipadairp.png);
    height:630px;
    width:540px;
    margin:0 auto
}

.ipadairps {
    height:930px;
    width:768px;
    margin:127px 97px;
    border:none;
    -ms-zoom:.45;
    -moz-transform:scale(0.45);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.45);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.45);
    -webkit-transform-origin:0 0
}

#ipadairl {
    background-image:url(img/ipadairl.png);
    height:450px;
    width:630px;
    margin:0 auto
}

.ipadairls {
    height:675px;
    width:1024px;
    margin:94px 84px;
    border:none;
    -ms-zoom:.45;
    -moz-transform:scale(0.45);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.45);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.45);
    -webkit-transform-origin:0 0
}

/* iPad Mini */
#ipadmp {
    background-image:url(img/ipadmp.png);
    height:630px;
    width:540px;
    margin:0 auto
}

.ipadmps {
    height:920px;
    width:768px;
    margin:143px 108px;
    border:none;
    -ms-zoom:.423;
    -moz-transform:scale(0.423);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.423);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.423);
    -webkit-transform-origin:0 0
}

#ipadml {
    background-image:url(img/ipadml.png);
    height:450px;
    width:630px;
    margin:0 auto
}

.ipadmls {
    height:662px;
    width:1024px;
    margin:107px 99px;
    border:none;
    -ms-zoom:.421;
    -moz-transform:scale(0.421);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.421);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.421);
    -webkit-transform-origin:0 0
}

/* New Macbook */
#newmacbook {
    background-image:url(img/newmacbook.png);
    height:530px;
    width:900px;
    margin:0 auto;
}

.newmacbooks {
    height:618px;
    width:1152px;
    margin:117px 136px;
    border:none;
    -ms-zoom:.538;
    -moz-transform:scale(0.538);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.538);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.538);
    -webkit-transform-origin:0 0;
}
/* Macbook Air*/
#macbookair {
    background-image:url(img/macbookair.png);
    height:530px;
    width:900px;
    margin:0 auto
}

.macbookairs {
    height:675px;
    width:1153px;
    margin:98px 161px;
    border:none;
    -ms-zoom:.5;
    -moz-transform:scale(0.5);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.5);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.5);
    -webkit-transform-origin:0 0
}

/* Macbook */
#macbook {
    background-image:url(img/macbook.png);
    height:530px;
    width:900px;
    margin:0 auto
}

.macbooks {
    height:700px;
    width:1280px;
    margin:106px 130px;
    border:none;
    -ms-zoom:.5;
    -moz-transform:scale(0.5);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.5);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.5);
    -webkit-transform-origin:0 0
}

/* Macbook 15 */
#macbook15 {
    background-image:url(img/macbook.png);
    height:530px;
    width:900px;
    margin:0 auto
}

.macbook15s {
    height:800px;
    width:1440px;
    margin:106px 130px;
    border:none;
    -ms-zoom:.444;
    -moz-transform:scale(0.444);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.444);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.444);
    -webkit-transform-origin:0 0
}

/* iMac 21.5 */
#imac {
    background-image:url(img/imac.png);
    height:630px;
    width:700px;
    margin:0 auto
}

.imacs {
    height:983px;
    width:1920px;
    margin:103px 30px;
    border:none;
    -ms-zoom:.3333333;
    -moz-transform:scale(0.3333333);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.3333333);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.3333333);
    -webkit-transform-origin:0 0
}

/* iMac 27 */
#imac2 {
    background-image:url(img/imac.png);
    height:630px;
    width:700px;
    margin:0 auto 150px
}

.imac2s {
    height:1310px;
    width:2560px;
    margin:103px 30px;
    border:none;
    -ms-zoom:.25;
    -moz-transform:scale(0.25);
    -moz-transform-origin:0 0;
    -o-transform:scale(0.25);
    -o-transform-origin:0 0;
    -webkit-transform:scale(0.25);
    -webkit-transform-origin:0 0
}




#mobile {
    display:none
}

#about a {
    color:#e64d3d;
    text-decoration:none
}

#about a:hover {
    color:#000
}

.small {
    margin:30px 0;
    font-size:14px
}

.creator {
    padding-top:10px;
    text-align:center;
    font-size:14px
}
.creator i{
	margin:5px 2px 0;
}

#smallscreen {
    display:none
}

@media (max-width: 1186px) {
#nav {
    font-size:14px
}

#nav a {
    padding-left:18px;
    padding-right:18px
}


.fnav {
    padding:10px 18px;
}
.pagelink {
    margin-top:0;
    font-size:14px
}
}

@media (max-width: 991px) {
#form {
    display:none
}

#homepng {
    height:auto;
    width:90%;
    margin-left:5%;
    margin-bottom:10px
}

.homepng {
    height:auto;
    width:90%;
    margin-left:5%
}

h1 {
    margin-top:0
}

#mobile {
    display:block;
    text-decoration:none;
    text-align:centre;
    width:90%;
    margin-left:5%
}

#smallscreen {
    display:block
}



}
</style>			  
		 
<div id="nav">
<ul>
<li><a data-scroll="" href="#about" class="fnav">Merci de choisir le téléphone ou l'écran à afficher</a></li>

</ul>
<div id="navi">
<ul class="pagelink">
<li><a data-scroll="" href="#titlei6p">iPhone</a>
<ul>
<li><a data-scroll="" href="#titlei6p">iPhone 6 Plus</a></li>
<li><a data-scroll="" href="#titlei6">iPhone 6</a></li>
<li><a data-scroll="" href="#titlei5">iPhone SE</a></li>
<li><a data-scroll="" href="#titlei4">iPhone 4</a></li>
</ul>
</li>
<li><a data-scroll="" href="#titleipp">iPad</a>
<ul>
<li><a data-scroll="" href="#titleipp">iPad Pro</a></li>
<li><a data-scroll="" href="#titleip">iPad Air 2</a></li>
<li><a data-scroll="" href="#titleipm">iPad Mini 4</a></li>
</ul>
</li>
<li><a data-scroll="" href="#titlembair">Macbook</a>
<ul>
<li><a data-scroll="" href="#titlembair">Macbook Air 11"</a></li>
<li><a data-scroll="" href="#titlenmb">Macbook 12"</a></li>
<li><a data-scroll="" href="#titlemb13">Macbook Pro 13"</a></li>
<li><a data-scroll="" href="#titlemb15">Macbook Pro 15"</a></li>
</ul>
</li>
<li><a data-scroll="" href="#titleim">iMac</a>
<ul>
<li><a data-scroll="" href="#titleim">iMac 21.5"</a></li>
<li><a data-scroll="" href="#titleim2">iMac 27"</a></li>
</ul>
</li>
</ul>
</div>
</div>
<div id="titlei6p"><h3>iPhone 6 Plus</h3></div>
<div id="iphone6pp">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone6pps"></iframe>
</div>
<div id="iphone6pl">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone6pls"></iframe>
</div>
<div id="titlei6"><h3>iPhone 6</h3></div>
<div id="iphone6p">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone6ps"></iframe>
</div>
<div id="iphone6l">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone6ls"></iframe>
</div>
<div id="titlei5"><h3>iPhone 5SE</h3></div>
<div id="iphone5p">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone5ps"></iframe>
</div>
<div id="iphone5l">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone5ls"></iframe>
</div>
<div id="titlei4"><h3>iPhone 4</h3></div>
<div id="iphone4p">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone4ps"></iframe>
</div>
<div id="iphone4l">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="iphone4ls"></iframe>
</div>
<div id="titleipp"><h3>iPad Pro</h3></div>
<div id="ipadpp">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="ipadpps"></iframe>
</div>
<div id="ipadpl">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="ipadpls"></iframe>
</div>
<div id="titleip"><h3>iPad Air 2</h3></div>
<div id="ipadairp">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="ipadairps"></iframe>
</div>
<div id="ipadairl">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="ipadairls"></iframe>
</div>
<div id="titleipm"><h3>iPad Mini 4</h3></div>
<div id="ipadmp">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="ipadmps"></iframe>
</div>
<div id="ipadml">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="ipadmls"></iframe>
</div>
<div id="titlembair"><h3>Macbook Air 11"</h3></div>
<div id="macbookair">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="macbookairs"></iframe>
</div>
<div id="titlenmb"><h3>Macbook 12"</h3></div>
<div id="newmacbook">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="newmacbooks"></iframe>
</div>
<div id="titlemb13"><h3>Macbook 13"</h3></div>
<div id="macbook">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="macbooks"></iframe>
</div>
<div id="titlemb15"><h3>Macbook Pro Retina 15"</h3></div>
<div id="macbook15">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="macbook15s"></iframe>
</div>
<div id="titleim"><h3>iMac 21.5"</h3></div>
<div id="imac">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="imacs"></iframe>
</div>
<div id="titleim2"><h3>iMac 27"</h3></div>
<div id="imac2">
    <iframe src="<?php echo $_REQUEST["site"] ; ?>" class="imac2s"></iframe>
</div>
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
            </div><!-- end container -->
        </section><!-- end section -->
<?php 
include ("footer.php");

?>

    