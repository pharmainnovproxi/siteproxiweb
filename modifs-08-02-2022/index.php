<?php
include ("header.php");
?>
  <title>PROXIWEB - Agence Tunisienne Création Sites Internet & Marketing</title>
  <meta name="description" content="Commandez votre modèle web et Choisissez votre site en quelques clics, Découvrez un échantillon de nos créations de sites Internet
  vitrine, sur mesure, e-commerce ou progiciel.">
        <div class="first-slider">
            <div id="rev_slider_211_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webproductlighthero" style="background-color:#1D4BAD;margin:0px auto;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.1.1RC fullwidth mode -->
                <div id="rev_slider_211_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.1.1RC">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-699" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="Intro" data-description="">
	                    <a href="modele.php">                          
						  <!-- MAIN IMAGE -->
                            <img src="upload/transparent.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-6" id="slide-699-layer-1" data-x="['right','right','center','center']" data-hoffset="['-254','-453','70','60']" data-y="['middle','middle','middle','bottom']" data-voffset="['30','50','211','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2500" data-responsive_offset="on" style="z-index: 5;">
							
							
							<img src="upload/macbookpro.png" alt="" width="1000" height="600" data-ww="['1000px','1000px','500px','350px']" data-hh="['600px','600px','300px','210px']" data-no-retina>
                            
							
							</div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-6 tp-videolayer" id="slide-699-layer-11" data-x="['left','left','left','left']" data-hoffset="['829','866','297','185']" data-y="['top','top','top','top']" data-voffset="['197','217','582','514']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3350" data-responsive_offset="on" data-videocontrols="none" data-videowidth="['653px','653px','325px','229px']" data-videoheight="['408px','408px','204px','145px']" data-videoposter="upload/traincover.jpg" data-videomp4="upload/Broadway.mp4" data-posterOnMObile="off" data-videopreload="auto" data-videoloop="loop" data-autoplay="on" style="z-index: 6;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-5" id="slide-699-layer-12" data-x="['left','left','left','left']" data-hoffset="['543','543','110','155']" data-y="['top','top','top','top']" data-voffset="['17','17','500','474']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 7;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-600" data-xe="600" data-ys="0" data-ye="0"><img src="upload/cloud1.png" alt="" width="500" height="273" data-ww="['500px','500px','250','125px']" data-hh="['273px','273px','137','68px']" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4" id="slide-699-layer-3" data-x="['left','left','center','center']" data-hoffset="['593','633','-110','-60']" data-y="['top','top','top','bottom']" data-voffset="['183','203','590','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2750" data-responsive_offset="on" style="z-index: 8;"><img src="upload/ipad.png" alt="" width="430" height="540" data-ww="['430px','430px','200px','170px']" data-hh="['540px','540px','251px','213px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-4" id="slide-699-layer-4" data-x="['left','left','left','center']" data-hoffset="['663','703','212','-60']" data-y="['top','top','top','bottom']" data-voffset="['271','291','632','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3700" data-responsive_offset="on" style="z-index: 9;"><img src="upload/express_ipad_content1.jpg" alt="" width="290" height="374" data-ww="['290px','290px','135px','115px']" data-hh="['374px','374px','174px','148px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 6 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-3" id="slide-699-layer-13" data-x="['left','left','left','left']" data-hoffset="['294','294','116','97']" data-y="['top','top','top','top']" data-voffset="['532','532','745','641']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 10;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-400" data-xe="400" data-ys="0" data-ye="0"><img src="upload/cloud2.png" alt="" width="600" height="278" data-ww="['600px','600px','300','150']" data-hh="['278px','278px','139','70']" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 7 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-699-layer-5" data-x="['left','left','left','left']" data-hoffset="['530','553','127','58']" data-y="['top','top','top','top']" data-voffset="['278','297','622','529']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="3000" data-responsive_offset="on" style="z-index: 11;"><img src="upload/iphone.png" alt="" width="260" height="450" data-ww="['260px','260px','130px','100px']" data-hh="['450px','450px','225px','173px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 8 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-699-layer-6" data-x="['left','left','left','left']" data-hoffset="['576','598','150','75']" data-y="['top','top','top','top']" data-voffset="['360','379','663','560']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3950" data-responsive_offset="on" style="z-index: 12;"><img src="upload/express_iphone_content1.jpg" alt="" width="170" height="286" data-ww="['170px','170px','85px','66px']" data-hh="['286px','286px','143px','111px']" data-no-retina>
                            </div>

                            <!-- LAYER NR. 9 -->
                            <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-699-layer-14" data-x="['left','left','left','left']" data-hoffset="['280','280','-10','-1']" data-y="['top','top','top','top']" data-voffset="['223','223','569','518']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 13;">
                                <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-200" data-xe="200" data-ys="0" data-ye="0"><img src="upload/cloud3.png" alt="" width="738" height="445" data-ww="['600px','600px','300','150']" data-hh="['278px','278px','181','90']" data-no-retina>
                                </div>
                            </div>

                            <!-- LAYER NR. 10 -->
                            <div class="tp-caption WebProduct-Title   tp-resizeme rs-parallaxlevel-7" id="slide-699-layer-7" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['0','0','177','160']" data-fontsize="['90','90','75','60']" data-lineheight="['90','90','75','60']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 14; white-space: nowrap;">
							Site internet
                                <br/>Sur devis
                            </div>

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption WebProduct-Content   tp-resizeme rs-parallaxlevel-7" id="slide-699-layer-9" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['129','127','365','314']" data-fontsize="['16','16','16','14']" data-lineheight="['24','24','24','22']" data-width="['448','356','370','317']" data-height="['none','none','81','88']" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 15; min-width: 448px; max-width: 448px; white-space: normal;">
							Faites décoller votre business sur le web  <br/>PROXIWEB-Basic 250 DT & PROXIWEB-Premium 750 DT 
                                <br/> Nous vous accompagnons à la création de votre site web
                            </div> 
							<!--
                            <div onclick="window.location.href='modele.php'" class="tp-caption rev-btn rev-btn  rs-parallaxlevel-8" id="slide-699-layer-8" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['228','228','456','400']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);bg:rgba(255, 255, 255, 1.00);" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1750" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]' data-responsive_offset="on" data-responsive="off" style="z-index: 16; white-space: nowrap; font-size: 16px; line-height: 48px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Raleway;background-color:rgba(51, 51, 51, 1.00);padding:0px 40px 0px 40px;border-color:rgba(0, 0, 0, 1.00);border-width:2px;letter-spacing:1px;">
							<a href="modele.php" >Découvrez nos modèles</a>	
                            </div>
							!-->
					</a>	
                        </li>
                    </ul>
                    <div class="tp-static-layers">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption -   tp-static-layer" id="slide-102-layer-1" data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']" data-y="['top','top','top','top']" data-voffset="['30','30','30','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="500" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"toggleclass","layer":"slide-102-layer-1","delay":"0","classname":"open"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-3","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-4","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-5","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-6","delay":"0"}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-startslide="-1" data-endslide="-1" style="z-index: 5; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);">
                            <div id="rev-burger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>

              <section class="section nopadtop lb">
            <div class="container-fluid">
                <div class="row center-tab">
                    <div class="panel with-nav-tabs panel-primary">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#buydomain" data-toggle="tab">Acheter un domaine</a></li>
                                <li><a href="#emailservice" data-toggle="tab">Acheter un Email Pro</a></li>
                                <li><a href="#wordpresshost" data-toggle="tab">Hébergement Web‎</a></li>
                            </ul>
                        </div>
                        <div class="panel-body container">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="buydomain">
                                    <div class="row">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <div class="wb">
                                                <div class="ribbon-wrapper-green"><div class="ribbon-green">-10% Promo</div></div>
                                                <div class="big-title">
                                                    <h3>
                                                    Vous recherchez une qualité premium<br>
                                                    <span>Nom de domaine ?</span>
                                                    </h3>
                                                </div><!-- end big-title -->

                                                <form class="checkdomain form-inline"  action="https://hosting.proxiweb.tn" method="get">
                                                    <div class="checkdomain-wrapper">
                                                        <p>
                                                        Avec notre superbe formulaire de recherche de nom de domaine, vous pouvez rechercher n'importe quel <strong>nom de domaine</strong> avec plusieurs extensions, par exemple .fr .com .net .org et plus.</p>
                                                        <div class="form-group">
                                                             <label class="sr-only" for="domainnamehere">Nom de domaine</label>
                                                             <input type="text" name="query" class="form-control" id="domainnamehere" placeholder="Entrer nom de domaine ici..">
                                                             <input type="hidden" name="a" value="add" >
															 <input type="hidden" name="domain" value="register" >
														     <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Chercher</button>
                                                        </div>
                                                        <hr>
                                                        <div class="clearfix"></div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domaintn" type="checkbox" class="styled" checked>
                                                            <label for="domaintn">.tn</label>
                                                        </div>
														     <div class="checkbox checkbox-warning">
                                                            <input id="domaincom" type="checkbox" class="styled" checked>
                                                            <label for="domaincom">.com</label>
                                                        </div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domainnet" type="checkbox" class="styled" checked>
                                                            <label for="domainnet">.net</label>
                                                        </div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domainorg" type="checkbox" class="styled">
                                                            <label for="domainorg">.org</label>
                                                        </div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domaintv" type="checkbox" class="styled">
                                                            <label for="domaintv">.tv</label>
                                                        </div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domaininfo" type="checkbox" class="styled">
                                                            <label for="domaininfo">.info</label>
                                                        </div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domainco" type="checkbox" class="styled">
                                                            <label for="domainco">.co</label>
                                                        </div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domainus" type="checkbox" class="styled">
                                                            <label for="domainus">.us</label>
                                                        </div>
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domaincc" type="checkbox" class="styled">
                                                            <label for="domaincc">.cc</label>
                                                        </div>
                                                     
                                                        <div class="checkbox checkbox-warning">
                                                            <input id="domainsale" type="checkbox" class="styled">
                                                            <label for="domainsale">.sale</label>
                                                        </div>
                                                    </div><!-- end checkdomain-wrapper -->
                                                </form>
                                            </div>
                                        </div><!-- end col -->

                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="banner">
                                                <img src="upload/banner_01.jpg" alt="" class="img-responsive">
                                                <div class="banner-desc">
                                                    <h3>Transferer votre propre<br>
                                                    <span>Nom Domaine </span>
                                                    </h3>
                                                </div>
                                                <div class="banner-button">
                                                    <a href="contact.php" class="btn btn-primary btn-sm">Transferer maintenant</a>
                                                </div>  
                                            </div><!-- end banner -->
                                        </div><!-- end col -->
                                    </div><!-- end row -->
                                </div><!-- end tab-pane -->

                                <div class="tab-pane fade" id="emailservice">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="wb">
                                                <div class="big-title">
                                                    <h3>Messagerie professionnelle<br>
                                                    <span>Utilisez votre domaine E-mail</span>
                                                    </h3>
                                                </div><!-- end big-title -->

                                                <div class="email-widget">
                                                    <p>
Toutes les adresses IP utilisées dans le système ne sont pas incluses dans le système d'adresse IP propre de la liste noire, de sorte qu'aucun spam ne tombe sur votre système.</p>
                                                    <ul class="check-list">
                                                        <li>Prise en charge POP / IMAP / Webmail</li>
                                                        <li>Limite quotidienne de 2500 e-mails envoyés</li>
                                                        <li>Protection Anti-spam </li>
                                                        <li>Tous les appareils mobiles pris en charge</li>
                                                        <li>Application Iphone / Android</li>
                                                    </ul><!-- end check -->
                                                </div><!-- end email widget -->
                                            </div><!-- end wb -->
                                        </div><!-- end col -->

                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="banner">
                                                <img src="upload/banner_02.jpg" alt="" class="img-responsive">
                                                <div class="banner-button">
                                                    <a href="#" class="btn btn-primary">Acheter Email Pro</a>
                                                </div>  
                                            </div><!-- end banner -->
                                        </div><!-- end col -->
                                    </div><!-- end row -->
                                </div><!-- end tab-pane -->

                                <div class="tab-pane fade" id="wordpresshost">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="wb">
                                                <div class="big-title">
                                                    <h3>Meilleur CM system<br>
                                                    <span>PROXIWEB CMS</span>
                                                    </h3>
                                                </div><!-- end big-title -->

                                                <div class="email-widget">
                                                    <p>PROXIWEB CMS  est une application web dont vous pouvez utiliser pour créer un beau site Web </p>
                                                    <ul class="check-list">
                                                        <li>Construire un site Pro</li>
                                                        <li>Meilleurs templates et modules</li>
                                                        <li>Sites performants et rapides</li>
                                                        <li>Multi Site & Support Sousdomaines </li>
                                                        <li>Documentations incluses</li>
                                                    </ul><!-- end check -->
                                                </div><!-- end email widget -->
                                            </div><!-- end wb -->
                                        </div><!-- end col -->

                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="flat white-style">
                                                <ul class="plan plan1">
                                                    <li class="plan-name">
                                                    PROXIWEB CMS Package
                                                    </li>
                                                    <li class="plan-price">
                                                    <strong>Apartir 80 DT TTC</strong> / an
                                                    </li>
                                                    <li>
                                                    <strong>Modèles</strong> par métier
                                                    </li>
                                                    <li>
                                                    <strong>Logo</strong> personalisé
                                                    </li>
                                                    <li>
                                                    <strong>Nom domaine</strong> inclus
                                                    </li>
                                                    <li>
                                                    <strong>1</strong> Email  Pro
                                                    </li>
                                                    <li>
                                                    <strong>Support</strong> par téléphone
                                                    </li>
                                                    <li class="plan-action">
                                                    <a href="https://hosting.proxiweb.tn" class="btn btn-primary">Commander</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div><!-- end col -->

                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="wb">
                                                <div class="big-title">
                                                    <h3>Pack Moins cher<br>
                                                    <span>Site sur modèle</span>
                                                    </h3>
                                                </div><!-- end big-title -->

                                                <div class="email-widget">
                                                    <p> Nous offrons un site moderne et optimisé</p>
                                                    <ul class="check-list">
                                                        <li>Gestion de contenu</li> 
                                                        <li>Réception des demandes par téléphone</li>
                                                        <li>Réception des devis par formulaire</li>
                                                         <li>Présentiontion de la société</li>
														  <li>SEO et Référencement optimisé</li>
                                                    </ul><!-- end check -->
                                                </div><!-- end email widget -->
                                            </div><!-- end wb -->
                                        </div><!-- end col -->
										
										
                                    </div><!-- end row -->
                                </div><!-- end tab-pane -->
                            </div><!-- end tab-content -->
                        </div><!-- end panel-body -->
                    </div><!-- end panel -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->  


<section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Nos nouveaux <span>créations</span></h3>
                    <p class="last">
                        Quelques créations de sites internet professionnels<br>
L'ensemble de nos sites internet sont compatibles mobiles et tablettes
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                   
				   
				   
				   
                  
					
					
                               
<?php

 $sql1= "SELECT * 
         FROM `dev_site`  where   lien not like '%Model%' and etatposition='2' and imagebg!='' and date_expiration>now()   ORDER BY   date_ajout DESC   "; 
 
		 $dateajout='datemodified';
 
 mysqli_query($GLOBALS['con'],"SET NAMES 'utf8'");
	
	
 $res1=mysqli_query($GLOBALS['con'],$sql1) or die( "<br />".$sql1); 
  $nb1=mysqli_num_rows($res1); 	
 
				if($nb1!=0){
	 $limit=2;
	 $debut=0;
			$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : ''; 	 
			if($premier==""){$premier=0;} 
			$premier=$page*$limit; 
			$nb_total=$nb1;
			$limite=mysqli_query($GLOBALS['con'],"$sql1 limit $debut,$limit"); 
			
			$limit_str = "LIMIT  ". $page*$limit .",$limit"; 
			
			$result1 = mysqli_query($GLOBALS['con'],"$sql1 $limit_str");  
	}
	
	$date_service='';
	
		
														while (($tab=mysqli_fetch_array($result1)))
														{  
$lec="";
 $id_site=$tab['id_site'];	
$nomsite=$tab['nomsite'];	
$nom_activite=$tab['nom_activite'];		
$img=$tab['img'];
$imgmobile=str_replace('.jpg','_mobile.jpg', $tab['img']);
 $date=$tab['date'];
$permalink_modele=$tab['permalink_modele'];	 			
$mypermalink=$tab['mypermalink'];	 
$lien=$tab['lien'];	 	 
$imagebg=$tab['imagebg'];

 $type=$tab['type'];
  $description=$tab['description'];
  
    $presentation=$tab['presentation'];
    
$description = strip_tags($presentation);
$description = wordwrap($description, 100);
$description =mb_substr($description, 0, 300 ).'...';
  
    $prix=$tab['prix'];
if ($description=='') $description="Nous vous offrons la possibilité de créer un site Web à partir de notre solution pour vous permettre d'avoir un site Web ergonomique, dynamique, esthétique et adaptable à tous les besoins de vos clients. ";
if ($prix==240) $prix=250 ; else $prix=750 ;
	
?>					
				 
                        <div class="content wbp">
             
			 
			 <div class="row">
                    <div class="col-md-7 col-sm-12">
                        <div class="blog-image">
                            <div id="myCarousel<?php echo $id_site ; ?>" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                         <center><img src="https://www.proxiweb.tn/modeles/<?php echo $imagebg; ?>" alt="" style="width:450px ; height:350px;"></center>
                                    </div>
                                
                            
                                </div>

                                <a class="left carousel-control" href="#myCarousel<?php echo $id_site ; ?>" role="button" data-slide="prev">
                                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                                    <span class="sr-only">Prec</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel<?php echo $id_site ; ?>" role="button" data-slide="next">
                                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                                    <span class="sr-only">Suiv</span>
                                </a>
                            </div>
                        </div><!-- end blog-image --> 
                    </div><!-- end col -->
                    <div class="col-md-5 col-sm-7 col-xs-12">
                        <div class="shop-desc">
                                     <h3><a href="realisations_detail-<?php echo str_replace(".","_",$lien) ; ?>" ><?php echo $nomsite; ?></a></h3>
                         
							 
							 <span><a class="btn btn-info" href="https://proxiweb.tn/link.php?site=<?php echo $lien ; ?>" target="_blank"><?php echo $lien ; ?></a> </span>
                          
		<span><a class="btn btn-info" href="realisations_detail-<?php echo str_replace(".","_",$lien) ; ?>" >+ infos</a> </span>
            <br>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
					      <p style="text-align:justify;">
						 <?php echo $description; ?>
						 </p>
					 
                       <a class="btn btn-default" href="modeles-visualiser.php?site=https://www.<?php echo $lien ; ?>" target="_blank">Visualiser</a>
                            <div class="addwish">
                                <a href="modeles-visualiser-appareil.php?site=https://www.<?php echo $lien ; ?>"><i class="fa fa-heart-o"></i> Visualiser sur Téléphone, Tablette et PC</a>
                            </div><!-- end addw -->

                            <div class="shopmeta"> 
                           </div><!-- end shopmeta -->

                        </div><!-- end desc -->
                    </div><!-- end col -->
                </div>
			 
			 
			 
			 
			 
			 
                        </div>
						
					 
						
						
				 <?php
}
?>					
							
	 

                
                           

				   
				   
				   
				   
				   
<br>
                 <center>  <a href="nos-realisations.php" class="btn btn-primary btn-sm">Afficher Tous</a>   </center>
                 
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->





        <section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Caractéristiques <span>du Pack</span></h3>
                    <p class="last">
                        Si vous recherchez un package d'hébergement Web de qualité supérieure pour votre site Web, vous êtes au bon endroit! <br> Nous offrons un package d'hébergement Web convivial pour tous!
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="custom-service colorful">
                            <img src="images/icons/white_icon_01.png" alt="" class="wow fadeIn">
                            <h4>Interface de gestion</h4>
                           <a href="contact.php" class="btn btn-primary btn-sm">Contacter nous</a>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="custom-service colorful">
                            <img src="images/icons/white_icon_02.png" alt="" class="wow fadeIn">
                            <h4>Disponible 24/7 tous les jours</h4>
                            <a href="contact.php" class="btn btn-primary btn-sm">Contacter nous</a>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="custom-service colorful">
                            <img src="images/icons/white_icon_03.png" alt="" class="wow fadeIn">
                            <h4>250 DT / an seulement</h4>
                        <a href="contact.php" class="btn btn-primary btn-sm">Contacter nous</a>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="custom-service colorful">
                            <img src="images/icons/white_icon_04.png" alt="" class="wow fadeIn">
                            <h4>Optimisation SEO</h4>
                          <a href="contact.php" class="btn btn-primary btn-sm">Contacter nous</a>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section searchbanner1 lightversion">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Statistiques <span>PROXIWEB</span></h3>
                    <p class="last">
                        Let's see what other's say about HostHubs web hosting company!<br> 10.000+ customers can not be wrong!
                    </p>
                </div><!-- end section-title -->
                <div class="row services-list hover-services text-center">
                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-html5"></i>
                            <h3>Sites hébergés</h3>
                            <p class="stat-count">551</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-smile-o"></i>
                            <h3> Clients satisfaits</h3>
                            <p class="stat-count">492</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-envelope-o"></i>
                            <h3>Tickets résolus</h3>
                            <p class="stat-count">2214</p>
                        </div><!-- end box -->
                    </div>

                    <div class="col-md-3 col-sm-6 wow fadeIn">
                        <div class="box">
                            <i class="fa fa-at"></i>
                            <h3>Domaines vendus</h3>
                            <p class="stat-count">685</p>
                        </div><!-- end box -->
                    </div><!-- end col -->
                </div>
            </div><!-- end container -->
        </section><!-- end section -->
 
        <section class="smallsec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <h3>
                           Commencez à créer votre site Web rapidement dès aujourd'hui!
						</h3>
                    </div>
                    <div class="col-md-4 text-center">
                        <a href="#" class="btn btn-primary btn-lg">Commander en ligne</a>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

<section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Packs & <span>Tarifs</span></h3>
                    <p class="last">
                        Ci-dessous nos super offres Création Web . Vous pouvez sélectionner n'importe quel service  Web ci-dessous! <br> Si votre pack n'est pas répertorié ici, n'oubliez pas de consulter notre page <strong><a href="#">Nos Tarifs</a></strong> .
                    </p>
                </div><!-- end section-title -->

                <div class="row pricing-bigger">
                    <div class="col-md-3 col-sm-4 col-xs-12 pricing-choose nopadding pricing-border">
                        <div class="pricing-header colorprimary">
                            <p> Nos Tarifs </p>
                            <h3>Packs professionels & Fonctionalités</h3>
                        </div>
                        <div class="pricing-body">
                            <ul>
                                  <li>Site web multi-écrans</li>
								  <li>Design personnalisé</li>
								  <li>Interface de gestion</li>
                                  <li>Suivi des statistiques</li>
                                  <li>Traffic</li>
								  <li>Nombre de pages</li>
								  <li>Nombre de photos</li>
								  <li>Nom Domaine</li>
								  <li>Email Pro</li>
								  <li>Sécurité SSL</li>
								  <li>Installation Gratuite</li>
								  <li>Support Téléphonique</li>
								  <li>Multilangues</li>
								  <li>Avis Clients</li>
								  <li>Référencement SEO </li>
								  <li>Appel Mobile </li>
								  <li>Réseaux sociaux</li>
                            </ul><!-- end ul -->
                        </div><!-- end body -->
                    </div><!-- end col -->
                    <div class="col-md-3 col-sm-2 col-xs-12 pricing-plan nopadding text-center pricing-border">
                        <div class="pricing-header color1">
                            <p>250 DT HT / an </p>
                            <h3>Pack Site web Présence <br>PROXIWEB-Basic</h3>
                        </div>
                        <div class="pricing-body">
                            <ul>
							     <li><i class="fa fa-check"></i></li>
								  <li>Sur Modèle</li>
								   <li><i class="fa fa-check"></i></li>
								
									 <li><i class="fa fa-check"></i></li>
                                   <li>illimité</li>
								     <li>illimité</li>
									   <li>illimité</li>
								  <li><i class="fa fa-check"></i></li>
								   <li><i class="fa fa-check"></i></li>
								    <li><i class="fa fa-check"></i></li>
									 <li><i class="fa fa-check"></i></li>
									  <li><i class="fa fa-check"></i></li>
									  <li><i class="fa fa-close"></i></li>
									  <li><i class="fa fa-close"></i></li>
									 <li>Basic</li>
									 <li>Basic</li>
									 <li><i class="fa fa-close"></i></li>
                            </ul><!-- end ul -->
                        </div><!-- end body -->
                    </div><!-- end col -->

                    <div class="col-md-3 col-sm-2 col-xs-12 pricing-plan nopadding text-center pricing-border">
                        <div class="pricing-header color2">
                            <p>750 DT HT / an  </p>
                            <h3>Pack Site web Présence <br>PROXIWEB-Premium </h3>
                        </div>
                        <div class="pricing-body">
                            <ul>
                                <li><i class="fa fa-check"></i></li>
								<li>Sur Modèle</li>
										  <li><i class="fa fa-check"></i></li>
										  <li><i class="fa fa-check"></i></li>  
									      <li>illimité</li>
								          <li>illimité</li>
									      <li>illimité</li>
								          <li><i class="fa fa-check"></i></li>
								          <li><i class="fa fa-check"></i></li>
								          <li><i class="fa fa-check"></i></li>
									      <li><i class="fa fa-check"></i></li>
									      <li><i class="fa fa-check"></i></li>
									      <li><i class="fa fa-check"></i></li>
									      <li><i class="fa fa-check"></i></li>
										  <li>SEO Avancé</li>
										  <li>Optimisé</li>
									      <li>100 Fan Page Facebook</li>
                            </ul><!-- end ul -->
                        </div><!-- end body -->
                    </div><!-- end col -->

                   <div class="col-md-3 col-sm-2 col-xs-12 pricing-plan nopadding text-center pricing-border">
                        <div class="pricing-header color3">
                            <p>Sur Devis</p>
                            <h3>Pack Site web SURMESURE</h3>
                        </div>
                        <div class="pricing-body">
                            <ul>
                                 <li><i class="fa fa-check"></i></li>
								 <li>Sur Mesure</li>
							     <li><i class="fa fa-check"></i></li>
								 <li><i class="fa fa-check"></i></li>  
								 <li>illimité</li>
								 <li>illimité</li>
							      <li>illimité</li>
								  <li><i class="fa fa-check"></i></li>
								   <li><i class="fa fa-check"></i></li>
								    <li><i class="fa fa-check"></i></li>
									 <li><i class="fa fa-check"></i></li>
									 
									 
									 	 <li><i class="fa fa-check"></i></li>
									      <li><i class="fa fa-check"></i></li>
									      <li><i class="fa fa-check"></i></li>
									  <li>SEO Avancé</li>
									   <li>Optimisé</li>
									 
									 
                                     <li>100 Fan Page Facebook</li>
                            </ul><!-- end ul -->
                        </div><!-- end body -->
                    </div>
 <!-- end col -->

                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->


























        <section class="section nopadding lb clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-12 customsection hidden-sm hidden-xs">
                        <img src="upload/section_02.jpg" alt="images">
                        <div class="text_container">
                            <span class="logo"></span>
                            <h4>Hébergement<br>Support</h4>
                            <p>
Bienvenue sur PROXIWEB, nous créeons des sites internet vraiment beaux et incroyables. Cela peut être utilisé pour décrire ce que vous faites, comment vous le faites et pour qui vous le faites. Ne manquez pas aujourd'hui!</p>
                            <a href="#" class="btn btn-primary">Commandez votre site maintenant!</a>
                        </div>
                    </div>

                    <div class="col-md-7 col-sm-12">
                        <div class="box-feature-full clearfix">
                            <div class="vertical-elements">
                                <div class="box">
                                    <div class="icon-container alignleft">
                                        <img src="images/icons/features_box_01.png" alt="" class="img-responsive">
                                    </div>

                                    <div class="feature-desc">
                                        <h4>Nom de domaine</h4>
                                        <p>Choisir le bon nom de domaine pour votre entreprise</p>
                                    </div><!-- end desc -->
                                </div><!-- end featurebox -->

                                <hr>
                            
                                <div class="box">
                                    <div class="icon-container alignleft">
                                        <img src="images/icons/features_box_02.png" alt="" class="img-responsive">
                                    </div>

                                    <div class="feature-desc">
                                        <h4>Hébergement</h4>
                                        <p>Nous hébergeons votre site à moindre coût</p>
                                    </div><!-- end desc -->
                                </div><!-- end featurebox -->

                                <hr>

                                <div class="box">
                                    <div class="icon-container alignleft">
                                        <img src="images/icons/features_box_03.png" alt="" class="img-responsive">
                                    </div>

                                    <div class="feature-desc">
                                        <h4>Création Site en 24H</h4>
                                        <p>Site sur modèle à créer en moin de 24H</p>
                                    </div><!-- end desc -->
                                </div><!-- end featurebox -->
                            </div><!-- end vertical -->
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- end section -->

        <section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Sécurité <span>& Fonctionalités</span></h3>
                    <p class="last">
                       
Si vous recherchez un hébergement Web de qualité supérieure pour votre site Web, vous êtes au bon endroit!<br> 
Nous offrons un excellent pack d'hébergement Web convivial pour tous!
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-service">
                            <img src="images/icons/custom_icon_01.png" alt="" class="wow fadeIn">
                            <h4>Interface de gestion</h4>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-service">
                            <img src="images/icons/custom_icon_02.png" alt="" class="wow fadeIn">
                            <h4>Optimisation Mobile</h4>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-service">
                            <img src="images/icons/custom_icon_03.png" alt="" class="wow fadeIn">
                            <h4>Documentations & Guides</h4>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->
                </div>

                <hr class="invis1">

                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-service">
                            <img src="images/icons/custom_icon_04.png" alt="" class="wow fadeIn">
                            <h4>Paiement sécurisé</h4>
                         </div><!-- end custom-service -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-service">
                            <img src="images/icons/custom_icon_05.png" alt="" class="wow fadeIn">
                            <h4>Email Professionnel</h4>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="custom-service">
                            <img src="images/icons/custom_icon_06.png" alt="" class="wow fadeIn">
                            <h4>HTTPS SSL</h4>
                        </div><!-- end custom-service -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section searchbanner">
            <div class="container">
                <div class="section-title text-center">
                    <h3><span>Recevez nos mails</span> Soyez à jour</h3>
                    <p class="last">
                        Abonnez-vous à notre newsletter pour plus de codes de réduction, des offres quotidiennes 
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="widget clearfix text-center">
                            <form class="checkdomain form-inline">
                                <div class="checkdomain-wrapper">
                                    <div class="form-group">
                                        <label class="sr-only" for="domainnamehere-menu1">Domaine</label>
                                        <input type="text" class="form-control" id="domainnamehere-menu1" placeholder="Votre email ici..">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-envelope-o"></i> Envoyer</button>
                                    </div>
                                </div><!-- end checkdomain-wrapper -->
                            </form>
                        </div><!-- end widget -->
                    </div>
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

   
        <section class="section testibanner">
            <div class="container-fluid">
                <div class="section-title text-center">
                    <h3>Clients <span>satisfaits</span></h3>
                    <p class="last">
                       
Voyons ce que les clients disent de l'hébergement Web PROXIWEB!<br> 1.000+ clients satisfaits!
                    </p>
                </div><!-- end section-title -->

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row testimonials">
                            <div class="col-md-3 col-sm-6 m30">
                                <blockquote>
                                    <p class="clients-words"> L'un des meilleurs modèle que j'ai jamais trouvé, je souhaitais toujours un modèle comme celui-ci pour mon projet. j'aime vraiment ce bon travail!</p>
                                    <span class="clients-name text-primary">— Med</span>
                                    <img class="img-circle img-thumbnail" src="upload/client_01.png" alt="">
                                </blockquote>
                            </div>
                            <div class="col-md-3 col-sm-6 m30">
                                <blockquote>
                                    <p class="clients-words">PROXIWEB est une société d'hébergement puissante! Maintenant avec le formulaire de contact. Parfait! Beau design, excellent support. </p>
                                    <span class="clients-name text-primary">— Ines</span>
                                    <img class="img-circle img-thumbnail" src="upload/client_02.png" alt="">
                                </blockquote>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <blockquote>
                                    <p  class="clients-words"> C'est un site incroyable !. PROXIWEB est très facile à modifier pour un débutant et la qualité de conception excellente.</p>
                                    <span class="clients-name text-primary">— Sami</span>
                                    <img class="img-circle img-thumbnail" src="upload/client_03.png" alt="">
                                </blockquote>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <blockquote>
                                    <p  class="clients-words">Pack d'hébergement bien conçu et organisé! avec une bonne quantité de fonctionnalités disponibles. à recommandé!</p>
                                    <span class="clients-name text-primary">— Leyla</span>
                                    <img class="img-circle img-thumbnail" src="upload/client_04.png" alt="">
                                </blockquote>
                            </div>
                        </div>
                    </div><!--/.col-->  
                </div><!--/.row-->
            </div><!-- end container -->
        </section><!-- end section -->

        <section class="section overflow">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="big-title">
                            <h3>PILOTEZ VOTRE SITE INTERNET
<br>
                            <span>Mise à jour faciles & illimitées</span>
                            </h3>
                        </div><!-- end big-title -->

                        <div class="hfeatures">
                            <p>
							
						Modifiez vos textes et vos images autant de fois que vous le voulez, suivez les statistiques de votre site, ajoutez de nouveaux menus, c’est simple et instantané.
<br>
PROXIWEB crée des sites administrables pour vous permettre de mettre à jour vos contenus et faire évoluer votre projet dans le temps ...</p>

                            <ul class="work-elements">
                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                            <img src="images/jinfo-cms.jpg" width="208" height="94" alt="" class="img-responsive">
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->

                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                            <img src="images/google-logo.jpg" width="208" height="94" alt="" class="img-responsive">
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->

                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                            <img src="images/visa.jpg" width="208" height="94" alt="" class="img-responsive">
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->

                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                            <img src="images/facebook-logo.jpg" width="208" height="94" alt="" class="img-responsive">
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->

                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                            <img src="images/google-plus-logo.jpg" width="208" height="94" alt="" class="img-responsive">
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->

                                <li>
                                    <div class="box GrayScale">
                                        <div class="icon-container">
                                            <img src="images/cb.jpg" width="208" height="94" alt="" class="img-responsive">
                                        </div>
                                    </div><!-- end featurebox -->
                                </li><!-- end col -->
                            </ul>
                        </div><!-- end hfeatures -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->

            <div class="image-box hidden-sm hidden-xs wow slideInRight">
                <img alt="" src="upload/device_07.png">
            </div>
        </section><!-- end section -->


<?php 
include ("footer.php");

?>

    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->  
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="js/revslider_04.js"></script>