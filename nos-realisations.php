<?php
include ("header.php");
?>

  <title>Nos réalisations - PROXIWEB - Liste des sites</title>
    <meta name="description" content="Trouvez la liste des sites webs crées par PROXIWEB , Découvrez un échantillon de nos créations de sites Internet
vitrine, sur mesure, e-commerce ou progiciel.">


            <section class="section paralbackground page-banner" style="background-image:url('upload/page_banner_01.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->

        <div class="section page-title">
            <div class="container clearfix">
                <div class="title-area pull-left">
                    <h2>Nos réalisations <small>Trouvez parmi +1000 réalisations </small></h2>
                </div><!-- /.pull-right -->
                <div class="pull-right hidden-xs">
                    <div class="bread">
                        <ol class="breadcrumb">
                            <li><a href="index.php">Accueil</a></li>
                            <li class="active">Nos réalisations</li>
                        </ol>
                    </div><!-- end bread -->
                </div><!-- /.pull-right -->
            </div>
        </div><!-- end page-title -->
     
        <section class="section lb">
            <div class="container">
                <div class="row">
				
				
				       <div class="sidebar col-md-3 col-sm-12">
                        <div class="widget">
                            <div class="loginbox text-center" style="padding: 20px;">
                                <h3>Rechercher maintenant</h3> 
                                <form class="form-inline" method="post" action="nos-realisations.php">
                                    <input type="text" name="motcle" placeholder="Mots clés.." required class="form-control" />
                                    <input type="submit" value="Chercher" class="btn btn-primary btn-block" />
                                </form>         
                            </div><!-- end newsletter -->
                        </div><!-- end widget -->



<style>
.xnav-pills > li:hover a {
color: #000  !important;border-color: #fff  !important;background-color: #fff !important;
}
</style>


                        <div class="widget">
                            <div class="wbp">
                                <div class="small-title">
                                <h3>Par Catégories</h3>
                                <hr>
                                </div>
                                
                                <div class="related-posts">
								
							 <ul class="nav xnav-pills nav-stacked tree" style="text-align: left;">	
								
						<?php
	 
/*
 
   $sqlb="SELECT * FROM dev_activite WHERE    parent='0'   ";
   $resb=mysqli_query($GLOBALS['con'],$sqlb)  ; 
   if (($nbb=mysqli_num_rows($resb))>0)
   {
	     while(($tbb=mysqli_fetch_array($resb))!=NULL){
			$id_activite= $tbb["id_activite"];
			$nom_activite= $tbb["nom_activite"];
			 $permalink= $tbb["permalink"];
   $sqla="SELECT * FROM dev_activite WHERE    parent='$id_activite'   ";
   $resa=mysqli_query($GLOBALS['con'],$sqla)  ; 
   if (($nba=mysqli_num_rows($resa))>0)
   {
	   ?>
	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a"> <span  class="badge pull-right"><?php echo $nba; ?></span> <?php echo $nom_activite; ?> </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	   <?php
	     while(($tba=mysqli_fetch_array($resa))!=NULL){
			 $id_activite= $tba["id_activite"];
	   ?>                                             

 <li><a href="nos-realisations-<?php echo $tba['permalink']; ?>"><?php echo $tba['nom_activite']; ?></a></li>
 	   <?php
		 }
	   ?>  
 </ul>
 
                                                </li>
 	   <?php
		 }
 
		 }
   }
  */
	   ?>                     		
					

 
								
  <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">2</span> Transports </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-taxi">Taxi</a></li>
 	                                                

 <li><a href="nos-realisations-vtc">VTC</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">9</span> Entreprise &amp; Services </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                
        
 <li><a href="nos-realisations-plomberie">Plomberie</a></li>
 <li><a href="nos-realisations-electricite">Electricité</a></li>
 	 <li><a href="nos-realisations-serrurerie">Serrurerie</a></li>
  	 <li><a href="nos-realisations-demenagement">Déménagement</a></li>
    	 <li><a href="nos-realisations-nettoyage">Nettoyage</a></li>
	   	 <li><a href="nos-realisations-menuisier">Menuisier</a></li>
 <li><a href="nos-realisations-travaux-renovations">Travaux &amp; Rénovations</a></li>
 	                                                

 <li><a href="nos-realisations-architecte">Architecte</a></li>
 	                                                

 <li><a href="nos-realisations-voitures">Voitures</a></li>
 	 	  <li><a href="nos-realisations-auto-ecole">Auto école</a></li>                                                  

 <li><a href="nos-realisations-immobilier">Immobilier</a></li>
 	                                                

 <li><a href="nos-realisations-finance">Finance</a></li>
  <li><a href="nos-realisations-comptabilite">Comptabilité</a></li>	                                                
 <li><a href="nos-realisations-avocat">Avocat</a></li>
   <li><a href="nos-realisations-assurance">Assurance</a></li>
    
 <li><a href="nos-realisations-informatique">Informatique</a></li>
 	                                                

 <li><a href="nos-realisations-jardinier">Jardinier</a></li>
 	                                                

 <li><a href="nos-realisations-animaux-et-veterinaires">Animaux et Vétérinaires</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">10</span> Boutiques en ligne </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-beaute-et-bien-etre">Beauté et bien-être</a></li>
 	                                                

 <li><a href="nos-realisations-electromenager-et-deco">Électroménager &amp; Déco</a></li>
 	                                                

 <li><a href="nos-realisations-arts-et-travail-manuel">Arts et travail manuel</a></li>
 	                                                

 <li><a href="nos-realisations-bijoux-et-accessoires">Bijoux et accessoires</a></li>
 	                                                

 <li><a href="nos-realisations-interieurs-et-design">Design Intérieurs</a></li>
 	                                                

 <li><a href="nos-realisations-alimentation-et-boissons">Alimentation et Boissons</a></li>
 	                                                

 <li><a href="nos-realisations-sports">Sports</a></li>
 	                                                

 <li><a href="nos-realisations-enfants-et-bebes">Enfants et bébés</a></li>
 	                                                

 <li><a href="nos-realisations-livres-et-publications">Livres et publications</a></li>
 	                                                

 <li><a href="nos-realisations-animaux-de-compagnie">Animaux de compagnie</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">2</span> Mode &amp; Beauté </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-coiffure-et-beaute">Coiffure &amp; Beauté</a></li>
 	                                                

 <li><a href="nos-realisations-mode-et-accessoires">Mode &amp; Accessoires</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">5</span> Restauration </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-restaurants">Restaurants</a></li>
 	                                                

 <li><a href="nos-realisations-gastronomie">Gastronomie</a></li>
 	                                                

 <li><a href="nos-realisations-cafes">Cafés</a></li>
 	                                                

 <li><a href="nos-realisations-boulangeries">Boulangeries</a></li>
 	                                                

 <li><a href="nos-realisations-traiteurs">Traiteurs</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">2</span> Voyages et tourisme </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-hotels">Hôtels</a></li>
 	                                                

 <li><a href="nos-realisations-appartements">Appartements</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">3</span> Musique </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-artistes">Artistes</a></li>
 	                                                

 <li><a href="nos-realisations-dj-et-producteur">DJ et producteur</a></li>
 	                                                

 <li><a href="nos-realisations-musique">Musique</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "  ><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;" > <span class="badge pull-right">4</span> Photographie </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree" >
	                                                

 <li><a href="nos-realisations-photographie">Photographie</a></li>
 	                                                

 <li><a href="nos-realisations-portraits-et-evenements">Portraits et Événements</a></li>
 	                                                

 <li><a href="nos-realisations-presse">Presse</a></li>
 	                                                

 <li><a href="nos-realisations-reportages">Reportages</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">2</span> Communauté &amp; Éducation </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-ecole">Ecole</a></li>
 	                                                

 <li><a href="nos-realisations-associations">Associations</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">3</span> Événements </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-mariages-et-celebrations">Mariages &amp; Célébrations</a></li>
 	                                                

 <li><a href="nos-realisations-organisation-evenements">Organisation Événements</a></li>
 	                                                

 <li><a href="nos-realisations-salles-de-concert">Salles de concert</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">3</span> Santé et Bien-ètre </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-sport-et-loisirs">Sport &amp; Loisirs</a></li>
 	                                                

 <li><a href="nos-realisations-sante">Santé</a></li>
 	                                                

 <li><a href="nos-realisations-bien-etre">Bien-être</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">3</span> Arts Créatifs </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-arts-de-la-scene">Arts de la scàne</a></li>
 	                                                

 <li><a href="nos-realisations-art-et-litterature">Art et littérature</a></li>
 	                                                

 <li><a href="nos-realisations-arts-visuels">Arts visuels</a></li>
 	     
 </ul>
 
                                                </li>
 	   	 <li class=" dropdown-tree  open-tree active "><a class="dropdown-tree-a" style="color: #fff; border-color: #1D4BAD;  background-color: #1D4BAD;"> <span class="badge pull-right">3</span> Design </a>
     
	   	 <ul class="category-level-2 dropdown-menu-tree">
	                                                

 <li><a href="nos-realisations-designs">Design</a></li>
 	                                                

 <li><a href="nos-realisations-agences">Agences</a></li>
 	                                                

 <li><a href="nos-realisations-portfolios">Portfolios</a></li>
 	     
 </ul>
 
                                                </li>
 	                        		
								
								
                                     
                                </ul></div><!-- end related -->
                            </div><!-- end newsletter -->
                        </div>					
								
                                     
                       









 

                    </div><!-- end sidebar -->
				
				
				
				
				
                    <div class="col-md-9 col-sm-12">
					
					
                               
<?php
	if ( $_REQUEST['permalink']!='' ) 
 
{
	$permalink=$_REQUEST["permalink"] ;
 $sql0= "SELECT * FROM `dev_activite` where  permalink='$permalink'  "; 
 $res0=mysqli_query($GLOBALS['con'],$sql0) ;
 $tb=mysqli_fetch_array($res0) ;
 $id_activite=$tb['id_activite'];
 $nom_activite=$tb['nom_activite'];
 echo "<center><h3 >$nom_activite</h3></center><br>";
}

 $sql1= "SELECT * 
         FROM `dev_site` where  "; 
	if ( $id_activite!='' )  $sql1.=" /* ( nomsite like '%$nom_activite%' or presentation like '%$nom_activite%'  ) */ id_activite='$id_activite'   and ";
	if ( $_REQUEST['motcle']!='' )   $sql1.="  ( nomsite like '%$_REQUEST[motcle]%'  )  and  ";
		 $sql1.="  lien not like '%Model%' and etatposition='2' and imagebg!='' and date_expiration>now()   ORDER BY   date_ajout DESC  ";
		 $dateajout='datemodified';
  
	
	
 $res1=mysqli_query($GLOBALS['con'],$sql1) or die( "<br />".$sql1); 
  $nb1=mysqli_num_rows($res1); 	
 
				if($nb1!=0){
	 $limit=10;
	 $debut=0;
			$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : ''; 	 
			if($premier==""){$premier=0;} 
			$premier=$page*$limit; 
			$nb_total=$nb1;
			$limite=mysqli_query($GLOBALS['con'],"$sql1 limit $debut,$limit"); 
			
			$limit_str = "LIMIT  ". $page*$limit .",$limit"; 
			
			$result1 = mysqli_query($GLOBALS['con'],"$sql1 $limit_str");  
	
	
	$date_service='';
	
		
														while (($tab=mysqli_fetch_array($result1)))
														{  
$lec="";
 $id_site=$tab['id_site'];	
$nomsite=$tab['nomsite'];	
$nom_activite=$tab['nom_activite'];		
$img=$tab['img'];
$imgmobile=str_replace('.jpg','_mobile.jpg', $tab['img']);
 $date=$tab['date'];
$lien=$tab['lien'];	 	 
$imagebg=$tab['imagebg'];	
 $type=$tab['type'];
  $presentation=$tab['presentation'];
    
$description = strip_tags($presentation);
$description = wordwrap($description, 100);
$description =mb_substr($description, 0, 300 ).'...';
//$description = explode("\n", $description);
	
	
    $prix=$tab['prix'];
if ($description=='') $description="Nous vous offrons la possibilité de créer un site Web à partir de notre solution pour vous permettre d'avoir un site Web ergonomique, dynamique, esthétique et adaptable à tous les besoins de vos clients. ";
?>					
					
                        <div class="content wbp">
             
			 
			 <div class="row">
			
			
			<?php
			if ( $imagebg )
			{
			?>
			
			
			
                    <div class="col-md-7 col-sm-12">
                        <div class="blog-image">
                            <div id="myCarousel<?php echo $id_site ; ?>" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                         <center><img src="https://www.proxiweb.tn/modeles/<?php echo $imagebg; ?>" alt="" style="width:450px ; height:350px;"></center>
                                    </div>
                                 
                            
                                </div>

                                <a class="left carousel-control" href="#myCarousel<?php echo $id_site ; ?>" role="button" data-slide="prev">
                                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                                    <span class="sr-only">Prec</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel<?php echo $id_site ; ?>" role="button" data-slide="next">
                                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                                    <span class="sr-only">Suiv</span>
                                </a>
                            </div>
                        </div> 
                    </div> 
					<?php
			}
			?>	
					
                    <div class="col-md-5 col-sm-7 col-xs-12">
                        <div class="shop-desc">
                            <h3><a href="realisations_detail-<?php echo str_replace(".","_",$lien) ; ?>" ><?php echo $nomsite; ?></a></h3>
                         
							 
							 <span><a class="btn btn-info" href="https://proxiweb.tn/link.php?site=<?php echo $lien ; ?>" target="_blank"><?php echo $lien ; ?></a> </span>
                          
		<span><a class="btn btn-info" href="realisations_detail-<?php echo str_replace(".","_",$lien) ; ?>" >+ infos</a> </span>
                          
<br>
                            <div class="rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
							
                         <p style="text-align:justify;">
						 <?php echo $description; ?>
						 </p>
                                       
   <a class="btn btn-default" href="modeles-visualiser.php?site=https://www.<?php echo $lien ; ?>" target="_blank">Visualiser</a>
                            <div class="addwish">
                                <a href="modeles-visualiser-appareil.php?site=https://www.<?php echo $lien ; ?>"><i class="fa fa-heart-o"></i> Visualiser sur Téléphone, Tablette et PC</a>
                            </div><!-- end addw -->

                            <div class="shopmeta"> 
                           </div><!-- end shopmeta -->

                        </div><!-- end desc -->
                    </div><!-- end col -->
                </div>
			 
			 
			 
			 
			 
			 
                        </div>
						
						
						
						
				 <?php
}

}
else
	
	{
		echo "<center><h4>Aucune réalisation dans cette rubrique</h4></center>";
	}

?>					
							
						
						 		<center>	
        <nav class="content wbp">
                            <ul class="pagination">								
			<?php
	if ($page>0)
		 { 
		$precedent=$page-1; 
		?>
		  <li><a href="<?php echo $_SERVER[PHP_SELF]."?page=".$precedent."&".$dateajout."&".$typetaxi."&".$typepays."&".$typedepartement  ; ?>"><<</a></li>
		<?php
		} 
		$i=0; 
		$j=1; 
	
		if($nb_total>$limit) 
		{ 
		
		while($i<($nb_total/$limit)) 
		{ 
		if($i!=$page)
		{
		echo "  <li><a href='$PHP_SELF?page=$i&$dateajout&$typetaxi&$typepays&$typedepartement'>$j</a></li>";}
		else { echo "<li class='active'><a >$j</a></li>";} 
		$i++;$j++; 
		} 
		} 
		if(($premier+$limit)<$nb_total) 
		{ 
		$suivant=$page+1; 
		echo " <li><a href='$PHP_SELF?page=$suivant&$dateajout&$typetaxi&$typepays&$typedepartement' style=text-decoration:none;> >> </a></li>";
		
		} 
?>					

  
                            </ul>
                        </nav>	
		</center>

                
                           

                    </div><!-- end col -->

             
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
<?php 
include ("footer.php");

?>

     