<?php
include ("header.php");
?>

  <title>SMART ADSL TOPNET - PROXIWEB - Internet fixe Tunisie  </title>
    <meta name="description" content=" PROXIWEB : Une gamme de Offre Internet fixe SMART ADSL TOPNET pour entreprises et particuliers- TOPNET.">
 
            <section class="section paralbackground page-banner" style="background-image:url('upload/page_banner_02.jpg');" data-img-width="2000" data-img-height="400" data-diff="100">
        </section><!-- end section -->
		
		
        <section class="section">
            <div class="container">
                <div class="section-title text-center">
                    <h3> <div class="thr-icons-box-title" style="font-size:36px;">Internet fixe pour entreprises et particuliers</div></h3> <br>
					<br>
                    

<picture>
<source srcset="images/topnet.webp" type="image/webp">
<source srcset="images/topnet.jpg" type="image/jpg"> 
<img src="images/topnet.jpg" alt="" width="70%">
</picture>

					 
					
	       <br>
				   <br>
				   <a href="tel:71 891 521" class="btn btn-default btn-lg">Appelez 71 891 521</a>
                </div><!-- end section-title -->

 
    
    <section class="container fth-row" style="margin-top:45px;padding-left:0;padding-right:0;">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
     
	  
    <div class="wrapper">
            <div class="container page_offre avec_bordure">
<div class="row">
<div class="col-lg-6 col-md-6">
   <div class="thr-icons-box-title" style="font-size:24px;">ADSL TOPNET</div>		
<p style="text-align:justify">Choisissez votre connexion Internet selon votre niveau d’usage :<br>

Vous souhaitez mettre en place un accès Internet pour un de vos sites, nous vous proposons 3 technologies d’accès différentes adaptées à votre budget pour répondre à vos besoins en débit montant et descendant. Et pour vos besoins d'échanges importants de données, découvrez le très haut débit avec la fibre optique
</p><br>
 		
			
   <div class="thr-icons-box-title" style="font-size:24px;">DES OFFRES INTERNET CONÇUES POUR VOUS :</div>
                <div style="clear:both;"></div>
                <div class="thr-icons-box-desc" style="text-align:left;line-height:26px;padding-left:12px">
                    <b>Une connexion adaptée à vos usages :</b> SMART ADSL, VDSL, fibre optique<br>
                    <b>Une gamme de débits pour répondre aux besoins</b> de vos petits et très grands sites<br>
                    <b>Une disponibilité de service supérieure à 99% garantie</b> 
                </div>
				
				

<div class="content_offre">
<p>L’ADSL Relax est un abonnement qui vous permet de bénéficier d’une connexion internet haut débit, en illimitée.</p>

<h2>Avantages</h2>

<ul class="liste_offre">
 <li>Vous pouvez choisir le débit que vous souhaitez en fonction de votre besoin.</li>
 <li>Vous avez une offre adaptée à votre besoin et qui répond à votre budget.</li>
 <li>Le Modem routeur pour le Relax est gratuit.</li>
</ul>

<p><br>
 
</div>
</div>

<div class="col-lg-6 col-md-6 padding_left_0">
    

<picture>
<source srcset="images/topnetadslrelax.webp" type="image/webp">
<source srcset="images/topnetadslrelax.png" type="image/jpg"> 
<img alt="" class="img_offre img-responsive" src="images/topnetadslrelax.png">
</picture>

</div>
</div>
</div>
    
    

    <div class="container style_content">
<h2 class="sous_titre_blue">Offres avec modem Routeur</h2>

<table class="my_table table table-striped tableau_offre">
 <thead>
  <tr>
   <th>Nos Offres</th>
   <th>
   <div>4 Méga</div>
   </th>
   <th>
   <div>8 Méga</div>
   </th>
   <th>
   <div>12 Méga</div>
   </th>
   <th>
   <div>jusqu’à 20 Méga</div>
   </th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <th>Tarif ADSL Relax
   <div>( TTC/mois)</div>
   </th>
   <td>9,530&nbsp;<sup>DT</sup></td>
   <td>14,296&nbsp;<sup>DT</sup></td>
   <td>19,062&nbsp;<sup>DT</sup></td>
   <td>23,918&nbsp;<sup>DT</sup></td>
  </tr>
  <tr>
   <th>Tarif ADSL Relax
   <div>( TTC/an)</div>
   </th>
   <td>95,300&nbsp;<sup>DT</sup></td>
   <td>142,960&nbsp;<sup>DT</sup></td>
   <td>190,620&nbsp;<sup>DT</sup></td>
   <td>239,180&nbsp;<sup>DT</sup></td>
  </tr>
  <tr>
   <th>Tarif ADSL Tunisie Telecom
   <div>( TTC/mois)</div>
   </th>
   <td>13,667&nbsp;<sup>DT</sup></td>
   <td>18,163&nbsp;<sup>DT</sup></td>
   <td>23,918&nbsp;<sup>DT</sup></td>
   <td>28,683&nbsp;<sup>DT</sup></td>
  </tr>
 </tbody>
</table>

<h2 class="sous_titre_blue">Offres avec modem WIFI</h2>

<table class="my_table table table-striped tableau_offre">
 <thead>
  <tr>
   <th>Nos Offres</th>
   <th>
   <div>4 Méga</div>
   </th>
   <th>
   <div>8 Méga</div>
   </th>
   <th>
   <div>12 Méga</div>
   </th>
   <th>
   <div>jusqu’à 20 Méga</div>
   </th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <th>Tarif ADSL Relax Wifi
   <div>( TTC/mois)</div>
   </th>
   <td>11,509&nbsp;<sup>DT</sup></td>
   <td>16,275&nbsp;<sup>DT</sup></td>
   <td>20,951&nbsp;<sup>DT</sup></td>
   <td>25,806&nbsp;<sup>DT</sup></td>
  </tr>
  <tr>
   <th>Tarif ADSL Relax Wifi
   <div>( TTC/an)</div>
   </th>
   <td>115,090&nbsp;<sup>DT</sup></td>
   <td>162,750&nbsp;<sup>DT</sup></td>
   <td>209,510&nbsp;<sup>DT</sup></td>
   <td>258,060&nbsp;<sup>DT</sup></td>
  </tr>
  <tr>
   <th>Tarif ADSL Tunisie Telecom
   <div>( TTC/mois)</div>
   </th>
   <td>13,667&nbsp;<sup>DT</sup></td>
   <td>18,163&nbsp;<sup>DT</sup></td>
   <td>23,918&nbsp;<sup>DT</sup></td>
   <td>28,683&nbsp;<sup>DT</sup></td>
  </tr>
 </tbody>
</table>
</div>

<div class="container sans_bg panel_offre">
<div aria-multiselectable="true" class="panel-group panel_structure" id="accordion" role="tablist">
<div class="panel panel-default">
<div class="panel-heading header_blue_ciel" id="heading11" role="tab">
<h4 class="panel-title"><a aria-controls="collapse11" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="https://www.topnet.tn/offres/detail/acces-internet-/adsl-relax#collapse11" role="button">Contrôle parental&nbsp;<i class="my_icon icon_collapse">&nbsp;</i> </a></h4>
</div>

<div aria-labelledby="heading11" class="panel-collapse collapse" id="collapse11" role="tabpanel">
<div class="panel-body">
<p>Prix : 30DT TTC/an (3DT/mois +2 mois gratuits)</p>

<p>Afin de protéger vos enfants contre les risques des contenus inappropriés (atteinte aux mineurs, violence, etc.) TOPNET vous offre son nouveau service « Kid Secure », facile et simple à utiliser, qui vous permet de restreindre automatiquement l’accès de vos enfants à un contenu indésirable (Sites WEB, Photos, vidéos).</p>

<p>Le contrôle parental « Kid Secure » de TOPNET ne requiert l’installation d’aucun logiciel, l’activation et la désactivation de votre service « Kid Secure » se fait d’une manière simple et rapide à travers votre espace client au niveau du portail TOPNET.</p>
</div>
</div>
</div>
</div>
</div>

            </div> 
	 
	 
	 
	 
	   </div> 
    </section>
	
	
	 </div>
        </section><!-- end section -->
<?php 
include ("footer.php");

?>
 